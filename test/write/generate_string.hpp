#ifndef TEST_WRITE_GENERATE_STRING_HPP_
#define TEST_WRITE_GENERATE_STRING_HPP_

#include <nov/json/impl/string_generator.hpp>

template<typename T>
static std::string generate(const T& x) {
	Nov::Json::Impl::StringGenerator sg;
	Nov::Json::Template::writeValue(sg, x);
	return sg.result();
}

template<typename T>
static std::string generateOne(const T& x) {
	std::vector<T> v;
	v.push_back(x);
	return generate(v);
}

#endif // TEST_WRITE_GENERATE_STRING_HPP_
