#include <test/test.hpp>
#include <nov/json/template/support/multimap.hpp>

// Mutimap is always an array of pairs

BOOST_AUTO_TEST_CASE ( emptyTest ) {
	typedef std::multimap<int, int> Type;
	BOOST_CHECK(
		parseAndCheck<Type>(
			"[]",
			{}
		)
	);
}

BOOST_AUTO_TEST_CASE ( oneTest ) {
	typedef std::multimap<int, int> Type;
	BOOST_CHECK(
		parseAndCheck<Type>(
			R"([[1,5],[1,8],[6,4]])",
			{{1,5}, {1,8}, {6,4}}
		)
	);
}
