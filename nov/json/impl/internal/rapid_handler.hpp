#ifndef NOV_JSON_IMPL_INTERNAL_RAPID_HANDLER_HPP_
#define NOV_JSON_IMPL_INTERNAL_RAPID_HANDLER_HPP_

#include <stack>
#include <sstream>
#include <stdexcept>
#include <iostream>

#include <nov/json/handler.hpp>

#include "rapidjson/rapidjson.h"
#include "rapidjson/reader.h"

namespace Nov {
namespace Json {
namespace Impl {

typedef Parser::Handler Handler;
typedef Parser::Integer Integer;

typedef std::runtime_error Exception;

class RapidHandler {
	// It is said in documentation, that maximum allowed level is 20
	static const size_t maximumDepthAllowed = 20;
	static const size_t handlersLimit = maximumDepthAllowed;

	enum ContainerType {Object, Array, Any=Object};

	typedef std::stack<std::pair<ContainerType, Handler::Ptr> > stack_t;
	stack_t handlers;

	enum {Name, Value} state = Value;
	ContainerType currentType = Any;

	Handler& currentHandler() const { return *handlers.top().second; }

	template<typename T>
	void writeDebugValue(std::ostream& oss, const T& x) {
		oss << x;
	}

	void writeDebugValue(std::ostream& oss, const std::string& x) {
		oss << "'" << x << "'";
	}

	template<typename T>
	void p(T& x) {
		state = Name;
		if (!currentHandler().value(x)) {
			errorStream << "Unexpected value ";
			writeDebugValue(errorStream, x);
			error();
		}
	}

	template<typename T>
	static constexpr uint64_t const_max() {
		return std::numeric_limits<T>::max();
	}

	template<typename T>
	void i(T x) {
		state = Name;

		if (const_max<T>() > const_max<int64_t>() && static_cast<uint64_t>(x) > const_max<int64_t>()) {
			errorStream << "Value too large for int64 " << x;
			error();
		}

		Integer i(static_cast<int64_t>(x));
		if (!currentHandler().value(i)) {
			errorStream << "Unexpected value " << x;
			error();
		}
	}

	template<ContainerType t>
	void pushHandler(Handler::Ptr& x) {
		currentType = t;
		handlers.push( std::make_pair(t, std::move(x)) );
	}
	bool oneMoreHandlerAllowed() {
		return handlers.size() < handlersLimit;
	}
	void validateOneMoreHandlerAllowed() {
		if (!oneMoreHandlerAllowed()) {
			error("Reached maximum level");
		}
	}
	void newObject() {
		validateOneMoreHandlerAllowed();
		if (auto x = currentHandler().object()) {
			pushHandler<Object>(x);
			state = Name;
			return;
		}
		error("Unexpected object");
	}
	void newArray() {
		validateOneMoreHandlerAllowed();
		if (auto x = currentHandler().array()) {
			pushHandler<Array>(x);
			return;
		}
		error("Unexpected array");
	}
	void onParsed() {
		if (!currentHandler().onParsed()) {
			error("Unexpected structure end");
		}
		handlers.pop();
		if (!handlers.empty()) {
			currentType = handlers.top().first;
		}
	}
public:
	void setTop(Handler::Ptr& topHandler) {
		stack_t tmp;
		tmp.swap(handlers);
		pushHandler<Any>(topHandler);
	}

	void Null() {
		state = Name;
		if (!currentHandler().null()) {
			error("Unexpected null value");
		}
	}

	void Bool(bool b)  {
		p(b);
	}
	void Int(int v) {
		i(v);
	}
	void Uint(unsigned v)  {
		i(v);
	}
	void Int64(int64_t v) {
		i(v);
	}
	void Uint64(uint64_t v) {
		i(v);
	}
	void Double(double v) {
		p(v);
	}
	// The last bool is for copy
	void String(const char* s, rapidjson::SizeType l, bool) {
		std::string value(s, l);
		if (state == Name && currentType == Object) {
			state = Value;
			if (!currentHandler().key(value)) {
				errorStream << "Unexpected key '" << value << "'";
				error();
			}
		} else {
			p(value);
		}
	}
	void StartObject() {
		newObject();
	}
	void EndObject(rapidjson::SizeType) {
		onParsed();
	}
	void StartArray() {
		newArray();
	}
	void EndArray(rapidjson::SizeType) {
		onParsed();
	}
private:
	void error(const char* x) {
		throw Exception(x);
	}
	void error() {
		throw Exception(errorStream.str());
	}

	std::ostringstream errorStream;
};

} // namespace Impl
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_IMPL_INTERNAL_RAPID_HANDLER_HPP_
