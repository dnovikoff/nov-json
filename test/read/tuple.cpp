#include <test/test.hpp>
#include <test/write/generate_string.hpp>
#include <nov/json/template/support/tuple.hpp>
#include <nov/json/template/support/pair.hpp>
#include <nov/json/template/support/vector.hpp>

BOOST_AUTO_TEST_CASE( tupleOne ) {
	std::tuple<int> x;
	BOOST_REQUIRE( parseString("[6]", x) );
	BOOST_CHECK_EQUAL( std::get<0>(x), 6 );

	BOOST_CHECK( !parseString("[]", x) );
	BOOST_CHECK( !parseString("[6,7]", x) );
}

BOOST_AUTO_TEST_CASE( tupleRepeat ) {
	std::tuple<int, int, int> x;
	BOOST_REQUIRE(parseString("[6, 7, 8]", x));
	BOOST_CHECK_EQUAL(std::get<0>(x), 6);
	BOOST_CHECK_EQUAL(std::get<1>(x), 7);
	BOOST_CHECK_EQUAL(std::get<2>(x), 8);

	BOOST_CHECK(!parseString("[]", x));
	BOOST_CHECK(!parseString("[6]", x));
	BOOST_CHECK(!parseString("[6,7]", x));
	BOOST_CHECK(!parseString("[6,7,8,9]", x));
}

BOOST_AUTO_TEST_CASE( tupleAll ) {
	std::tuple<int, bool, std::string, double> x;
	BOOST_REQUIRE( parseString(R"([6,false,"hi",0.25])", x) );
	BOOST_CHECK_EQUAL( std::get<0>(x), 6 );
	BOOST_CHECK_EQUAL( std::get<1>(x), false );
	BOOST_CHECK_EQUAL( std::get<2>(x), "hi" );
	BOOST_CHECK_EQUAL( std::get<3>(x), 0.25 );
}

BOOST_AUTO_TEST_CASE( tupleComplex ) {
	std::tuple<std::pair<int, int>, bool, std::vector<std::string>, double> x;
	BOOST_REQUIRE( parseString(R"([[6,7],false,["hi"],0.25])", x) );
	BOOST_CHECK_EQUAL( std::get<0>(x).first, 6 );
	BOOST_CHECK_EQUAL( std::get<0>(x).second, 7 );
	BOOST_CHECK_EQUAL( std::get<1>(x), false );
	BOOST_REQUIRE_EQUAL( std::get<2>(x).size(), 1u);
	BOOST_REQUIRE_EQUAL( std::get<2>(x).back(), "hi");
	BOOST_CHECK_EQUAL( std::get<3>(x), 0.25 );
}
