#ifndef NOV_JSON_TEMPLATE_READ_OBJECT_HPP_
#define NOV_JSON_TEMPLATE_READ_OBJECT_HPP_

#include <type_traits>
#include <nov/json/template/enum.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<typename T, bool>
struct DefaultReadObject {
	typedef NothingTag Accepted;
};

template<typename T>
class ReadObject: public DefaultReadObject<T, true> {
};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_READ_OBJECT_HPP_
