#ifndef NOV_JSON_TEMPLATE_SUPPORT_PAIR_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_PAIR_HPP_

#include <utility>
#include <type_traits>

#include <nov/json/template/read_array.hpp>
#include <nov/json/template/type.hpp>
#include <nov/json/template/write.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<bool check, typename PassedType, typename FirstType, typename SecondType>
struct StaticPairDebug {
};

template<typename PassedType, typename FirstType, typename SecondType>
struct StaticPairDebug<true, PassedType, FirstType, SecondType> {
	static const bool value = true;
};

// We treat pair as an array of exactly two elements
template<typename T1, typename T2>
class ReadArray<std::pair<T1, T2 > > {
public:
	typedef Enum<T1, T2> Accepted;
	typedef std::pair<T1, T2> PairType;

	template<typename T>
	bool value(T& x) {
		const bool valid = isConvertable<T, T1>() || isConvertable<T, T2>();
		static_assert(valid,
			"Target type is not convertable to any of T1, T2"
		);

		const bool result = StaticPairDebug<valid, T, T1, T2>::value;
		++index_;
		if (index_ == 1) {
			return jsonToUserOrFalse(x, data_.first);
		}
		if (index_ == 2) {
			return jsonToUserOrFalse(x, data_.second);
		}
		return result;
	}

	bool validate() {
		return index_ == 2;
	}

	template<typename Tag>
	std::unique_ptr<Parser::Handler> create() {
		++index_;
		if (index_ == 1) {
			return Detail::createFor<T1, Tag>([this] (T1&& x) {
				data_.first = std::move(x);
				return true;
			});
		}
		if (index_ == 2) {
			return Detail::createFor<T2, Tag>([this] (T2&& x) {
				data_.second = std::move(x);
				return true;
			});
		}

		return nullptr;
	}

	PairType move() {
		return std::move(data_);
	}

private:
	size_t index_ = 0;
	PairType data_;
};

template<typename T1, typename T2>
struct Writer<std::pair<T1, T2> > {
	typedef std::pair<T1, T2> Type;

	template<typename Generator>
	static void write(Generator& g, const Type& x) {
		g.startArray();
		writeValue(g, x.first);
		writeValue(g, x.second);
		g.endArray();
	}
};

template<typename Pair>
struct PairRemoveConst {
	typedef typename std::remove_const<typename Pair::first_type >::type Type1;
	typedef typename std::remove_const<typename Pair::second_type>::type Type2;
	typedef std::pair<Type1, Type2> type;
};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_PAIR_HPP_
