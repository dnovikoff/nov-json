#include <string>
#include <utility>

#include <nov/json/template/support/pair.hpp>
#include <nov/json/template/support/optional.hpp>
#include <nov/json/template/support/vector.hpp>
#include <nov/json/template/create.hpp>

void compile() {
	typedef std::pair<std::string, boost::optional<double> > t1;
	typedef std::pair<int, std::vector<int> > t2;
	typedef std::pair<t1, t2> t3;

	t3 x;
	Nov::Json::Template::createHandler( x );
}
