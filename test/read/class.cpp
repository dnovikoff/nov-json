#include <test/test.hpp>

#include <nov/json/template/support/class.hpp>

struct Example {
	int first;
	int second;
	std::string other;
};

namespace Nov {
namespace Json {
namespace Template {

template<>
struct ClassMeta<Example> {
	typedef Enum<std::string, int> Accepted;

	template<typename F>
	static void apply(Example& x, F& f) {
		if (!f("first", x.first)) {
			return;
		}
		if (!f("second", x.second)) {
			return;
		}
		if (!f("other", x.other)) {
			return;
		}
	}
};

} // namespace Template
} // namespace Json
} // namespace Nov

BOOST_AUTO_TEST_CASE ( simpleTest ) {
	Example e;
	// Default object returned
	BOOST_CHECK( parseString("{}", e) );

	BOOST_REQUIRE( parseString( R"({"first":345, "second" : 999, "other": "Dmitri Novikov"})", e) );
	BOOST_CHECK_EQUAL( e.first, 345 );
	BOOST_CHECK_EQUAL( e.second, 999 );
	BOOST_CHECK_EQUAL( e.other, "Dmitri Novikov" );

	// Unknown field not passed
	BOOST_REQUIRE( !parseString( R"({"first":345, "second" : 999, "other": "Dmitri Novikov", "onmorefield": 0})", e) );

	// wrong type
	BOOST_REQUIRE( !parseString( R"({"first": "d", "second" : 999, "other": "Dmitri Novikov"})", e) );

	// one field less is ok. Will be default
	BOOST_REQUIRE( parseString( R"({"first":345, "second" : 999})", e) );
}

BOOST_AUTO_TEST_CASE ( complexTest ) {
	OneArray<Example> e;
	BOOST_CHECK( parseString("[{}]", e) );

	BOOST_REQUIRE( parseString( R"([{"first":345, "second" : 999, "other": "Dmitri Novikov"}])", e) );
	BOOST_CHECK_EQUAL( e->first, 345 );
	BOOST_CHECK_EQUAL( e->second, 999 );
	BOOST_CHECK_EQUAL( e->other, "Dmitri Novikov" );
}

// Subtype
struct Example1 {
	std::string name;
	Example data;
};

namespace Nov {
namespace Json {
namespace Template {

template<>
struct ClassMeta<Example1> {
	typedef Enum<std::string, Example> Accepted;

	template<typename F>
	static void apply(Example1& x, F& f) {
		if (!f("name", x.name)) {
			return;
		}
		if (!f("data", x.data)) {
			return;
		}
	}
};

} // namespace Template
} // namespace Json
} // namespace Nov

BOOST_AUTO_TEST_CASE ( subClassTest ) {
	Example1 e;
	BOOST_REQUIRE( parseString( R"({"name":"MyObject", "data":{"first":345, "second" : 999, "other": "Dmitri Novikov"}})", e) );
	BOOST_CHECK_EQUAL( e.name, "MyObject" );
	BOOST_CHECK_EQUAL( e.data.first, 345 );
	BOOST_CHECK_EQUAL( e.data.second, 999 );
	BOOST_CHECK_EQUAL( e.data.other, "Dmitri Novikov" );
}

