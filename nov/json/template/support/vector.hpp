#ifndef NOV_JSON_TEMPLATE_SUPPORT_VECTOR_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_VECTOR_HPP_

#include <vector>

// Follows default insert for arrays
#define NOV_JSON_TEMPLATE_CONTAINER_NAME std::vector
#include <nov/json/template/support/helpers/list_macro.hpp>

#endif // NOV_JSON_TEMPLATE_SUPPORT_VECTOR_HPP_
