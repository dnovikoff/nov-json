#ifndef NOV_JSON_HANDLER_HPP_
#define NOV_JSON_HANDLER_HPP_

#include <memory>
#include <string>
#include <nov/json/integer.hpp>

namespace Nov {
namespace Json {
namespace Parser {

/**
 * Interface for your parser
 */
class Handler {
public:
	typedef std::unique_ptr<Handler> Ptr;
	Handler() {
	}
	virtual ~Handler() {
	}
	Handler(const Handler&) = delete;
	Handler& opeartor(const Handler&) = delete;

	/**
	 * Parsing different types
	 * return false if it this is not the type expected
	 */
	// @{
	virtual bool value(Integer) = 0;
	virtual bool value(bool) = 0;
	virtual bool value(double) = 0;
	virtual bool value(std::string&) = 0;
	// Called when null passes
	virtual bool null() = 0;
	// Called on map key
	virtual bool key(std::string&) = 0;
	// @}

	// empty ptr means failure
	virtual Ptr object() = 0;
	// empty ptr means failure
	virtual Ptr array() = 0;

	/**
	 * Called when parsing for this object/array finished
	 * You can do some checks here, like checking size
	 * return false if the result does not match what you want
	 */
	virtual bool onParsed() = 0;
};

/**
 * Base parser to be inherited from
 * All operations fails by default
 */
class BaseHandler: public Handler {
public:
	bool value(Integer) override {
		return false;
	}
	bool value(bool) override {
		return false;
	}
	bool value(double) override {
		return false;
	}
	bool value(std::string&) override {
		return false;
	}
	bool null() override {
		return false;
	}
	bool key(std::string&) override {
		return false;
	}
	Ptr object() override {
		return Ptr();
	}
	Ptr array() override {
		return Ptr();
	}

	// Note that onParsed will return true by default
	bool onParsed() override {
		return true;
	}

protected:
	virtual ~BaseHandler() {
	}
};

} // namespace Parser
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_HANDLER_HPP_
