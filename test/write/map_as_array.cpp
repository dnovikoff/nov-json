#include "test.hpp"

#include <nov/json/template/support/map_as_array.hpp>

BOOST_AUTO_TEST_CASE ( map ) {
	std::map<int, int> m;
	BOOST_CHECK_EQUAL(generate(m), R"([])");
	m[11] = 1;
	BOOST_CHECK_EQUAL(generate(m), R"([[11,1]])");
	m[22] = 2;
	BOOST_CHECK_EQUAL(generate(m), R"([[11,1],[22,2]])");
	m[33] = 3;
	BOOST_CHECK_EQUAL(generate(m), R"([[11,1],[22,2],[33,3]])");
}
