#ifndef NOV_JSON_TEMPLATE_TYPE_HPP_
#define NOV_JSON_TEMPLATE_TYPE_HPP_

#include <type_traits>
#include <nov/json/integer.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<typename T>
using RemoveAll = typename std::remove_reference<typename std::remove_cv<T>::type>::type;

template<typename T1, typename T2>
using IsSame = std::is_same<RemoveAll<T1>, RemoveAll<T2> >;

struct NotConvertable {
};

struct Convertable {
};

template<typename U, bool = Parser::Integer::supports<U>()>
struct IntegerType: public NotConvertable {
};

template<typename U>
struct IntegerType<U, true> {
	inline static bool jsonToUser(const Parser::Integer& j, U& u) {
		return j.to(u);
	}
};

template<typename J, typename U>
struct JsonType: public NotConvertable {
};

template<typename U>
struct JsonType<Parser::Integer, U>: public IntegerType<U> {
};

template<typename J, typename U>
struct Type: public JsonType<J, U> {
};

template<typename T>
struct Type<T, T> {
};

struct Null {
};

template<typename J, typename U>
constexpr bool isConvertable() {
	return !std::is_base_of<NotConvertable, Type<RemoveAll<J>, RemoveAll<U> > >::value;
}

template<typename JsonType, typename UserType>
inline bool jsonToUser(JsonType& pt, UserType& ut) {
	return Type<RemoveAll<JsonType>, RemoveAll<UserType> >::jsonToUser(pt, ut);
}

template<typename T>
inline bool jsonToUser(T& pt, T& ut) {
	ut = std::move(pt);
	return true;
}

// Required for correct convert detection
template<>
struct Type<Null, nullptr_t> {
};

template<typename JsonType, typename UserType, bool enabled = isConvertable<JsonType, UserType>()>
struct TypeOrFalse {
	inline static bool jsonToUser(JsonType&, UserType&) {
		return false;
	}
};

inline bool jsonToUser(Null, nullptr_t) {
	return true;
}

template<typename JsonType, typename UserType>
struct TypeOrFalse<JsonType, UserType, true> {
	inline static bool jsonToUser(JsonType& j, UserType& u) {
		return Template::jsonToUser(j, u);
	}
};

template<typename JsonType, typename UserType>
static bool jsonToUserOrFalse(JsonType& pt, UserType& ut) {
	return TypeOrFalse<JsonType, UserType>::jsonToUser(pt, ut);
}

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_TYPE_HPP_
