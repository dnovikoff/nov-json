#ifndef NOV_JSON_TEMPLATE_CREATE_HPP_
#define NOV_JSON_TEMPLATE_CREATE_HPP_

#include <nov/json/template/details/handler.hpp>

namespace Nov {
namespace Json {
namespace Template {

namespace Detail {
template<typename T>
class StartHandler: public Parser::BaseHandler {
public:
	explicit StartHandler(T& x): data(&x) {
	}

	template<typename Tag>
	Ptr create() {
		return createFor<T, Tag>([this] (T&& x) {
			(*data) = std::move(x);
			return true;
		});
	}
	Ptr object() override {
		return create<ObjectTag>();
	}
	Ptr array() override {
		return create<ArrayTag>();
	}

private:
	T* data;
};

template<typename T, bool enabled = Detail::couldBe<T, ObjectTag>() || Detail::couldBe<T, ArrayTag>()>
class CreateStart {
public:
	static Parser::Handler::Ptr create(T&) {
		static_assert(!enabled, "Parsed type should be convertable to json container");
		return nullptr;
	}
};

template<typename T>
class CreateStart<T, true> {
public:
	static std::unique_ptr<Parser::Handler> create(T& data) {
		return std::unique_ptr<Parser::Handler>(new Detail::StartHandler<T>(data));
	}
};
} // namespace Detail

/**
 * Call this function to create handler for your container
 * to use with your parser
 */
template<typename T>
Parser::Handler::Ptr createHandler(T& data) {
	return Detail::CreateStart<T>::create(data);
}

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_CREATE_HPP_
