#include <test/test.hpp>

#include <nov/json/template/support/pair.hpp>
#include <nov/json/template/support/vector.hpp>
#include <nov/json/template/support/optional.hpp>

BOOST_AUTO_TEST_CASE( simple ) {
	std::pair<bool, int> x;
	BOOST_CHECK( parseString("[true,1]", x));
	BOOST_CHECK( parseString("[false,2]", x));
	BOOST_CHECK( !parseString("[2, false]", x));
}

BOOST_AUTO_TEST_CASE( complexFirst ) {
	std::pair<std::vector<bool>, int> x;
	BOOST_CHECK( parseString("[[],1]", x) );
	BOOST_CHECK( !parseString("[{},1]", x) );
	BOOST_CHECK( parseString("[[false,false,true],1]", x) );
	BOOST_CHECK( !parseString("[[false,false,true],[]]", x) );
	BOOST_CHECK( !parseString("[[false,false,true],{}]", x) );
}

BOOST_AUTO_TEST_CASE( complexSecond ) {
	std::pair<int, std::vector<bool> > x;
	BOOST_CHECK( parseString("[1,[]]", x) );
	BOOST_CHECK( !parseString("[1,{}]", x) );
	BOOST_CHECK( parseString("[1,[false,false,true]]", x) );
	BOOST_CHECK_EQUAL( x.second.size(), 3u);
}

BOOST_AUTO_TEST_CASE( complexBoth ) {
	typedef std::pair<std::string, boost::optional<double> > t1;
	typedef std::pair<int, std::vector<int> > t2;
	typedef std::pair<t1, t2> t3;
	t3 x;
	BOOST_CHECK( !parseString("[[],[]]", x) );
	BOOST_CHECK( parseString(R"([["hi",null],[1,[]]])", x) );
}

BOOST_AUTO_TEST_CASE( exactylyTwo ) {
	std::pair<int, bool> x;
	BOOST_CHECK( !parseString("[]", x) );
	BOOST_CHECK( !parseString("[1]", x) );
	BOOST_CHECK( !parseString("[1,2]", x) );
	BOOST_CHECK( !parseString("[1,true,1]", x) );
	BOOST_CHECK( !parseString("[1,true,false]", x) );
	BOOST_CHECK( !parseString("[true,1]", x) );
}
