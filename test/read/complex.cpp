#include <test/test.hpp>

#include <nov/json/template/support/list.hpp>
#include <nov/json/template/support/map_as_object.hpp>
#include <nov/json/template/support/vector.hpp>

BOOST_AUTO_TEST_CASE ( complexContainerTest ) {
	typedef std::list<int> list_t;
	typedef std::map<std::string, list_t> map_t;
	typedef std::vector<map_t> vec_t;

	vec_t x;
	BOOST_REQUIRE( parseString("[]", x) );
	BOOST_REQUIRE( x.empty() );
	BOOST_REQUIRE( !parseString("[1]", x) );

	BOOST_CHECK( !parseString("[[]]", x) );
	BOOST_CHECK( parseString("[{}]", x) );
	BOOST_CHECK( x.size() == 1u);
	BOOST_CHECK( x.back().empty() );

	BOOST_REQUIRE( parseString(R"([{"hello":[]}])", x) );
	BOOST_REQUIRE(x.size() == 1u);
	BOOST_REQUIRE(x.back().size() == 1u);
	BOOST_REQUIRE(x.back().begin()->first == "hello");
	BOOST_REQUIRE(x.back().begin()->second.empty());

	BOOST_REQUIRE( parseString(R"([{"hello":[4,2,3]}])", x) );

	BOOST_REQUIRE(x.size() == 1u);
	BOOST_REQUIRE(x.back().size() == 1u);
	const list_t& lst = x.back()["hello"];
	BOOST_CHECK( vequals(lst, {4, 2, 3}) );
}
