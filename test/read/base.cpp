#include <test/test.hpp>

BOOST_AUTO_TEST_CASE ( doubleTest ) {
	std::vector<double> x;
	BOOST_CHECK( parseString("[]", x) );

	BOOST_CHECK( parseString("[1.0,2.0]", x) );
	BOOST_CHECK( vequals(x, {1.0, 2.0}) );

	// special hack for integers as double
	BOOST_REQUIRE( parseString("[8.6,15,9.12]", x) );
	BOOST_CHECK( vequals(x, {8.6, 15, 9.12}) );
}

BOOST_AUTO_TEST_CASE ( boolTest ) {
	std::vector<bool> x;
	BOOST_CHECK( !parseString("[1]", x) );
	BOOST_CHECK( !parseString("[null]", x) );
	BOOST_CHECK( parseString("[true]", x) );
	BOOST_CHECK( parseString("[false]", x) );
	BOOST_CHECK( !parseString("[ture]", x) );
	BOOST_CHECK( !parseString("[truefalse]", x) );

	BOOST_CHECK( parseString("[true,false]", x) );
	BOOST_CHECK( vequals(x, {true, false}) );
}

BOOST_AUTO_TEST_CASE ( nullTest ) {
	std::vector<nullptr_t> x;
	BOOST_REQUIRE( parseString("[]", x) );
	BOOST_REQUIRE( x.empty() );

	BOOST_REQUIRE( parseString("[null]", x) );
	BOOST_CHECK_EQUAL( x.size(), 1u);

	BOOST_REQUIRE( parseString("[null, null, null ]", x) );

	BOOST_CHECK_EQUAL( x.size(), 3u);
}

BOOST_AUTO_TEST_CASE( doubleAsIntTest ) {
	std::vector<int> x;
	BOOST_REQUIRE( parseString("[6]", x ) );
	BOOST_REQUIRE_EQUAL( x.size(), 1u);
	BOOST_REQUIRE_EQUAL( x.back(), 6);
	BOOST_REQUIRE( !parseString("[6.5]", x ) );
}
