#ifndef NOV_JSON_IMPL_STRING_GENERATOR_HPP_
#define NOV_JSON_IMPL_STRING_GENERATOR_HPP_

#include <stdint.h>

#include <memory>

namespace Nov {
namespace Json {
namespace Impl {

class StringGenerator {
	struct ImplDeleter {
		void operator()(void*);
	};
	const std::unique_ptr<void, ImplDeleter> impl;

	StringGenerator(const StringGenerator&) = delete;
	StringGenerator& operator=(const StringGenerator&) = delete;
public:
	StringGenerator();

	void startObject();
	void endObject();
	void startArray();
	void endArray();

	void write(const char*);
	void write(const std::string&);
	void write(const int64_t);
	void write(const double);
	void write(const bool);

	void writeNull();

	std::string result() const;
};

} // namespace Impl
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_IMPL_STRING_GENERATOR_HPP_
