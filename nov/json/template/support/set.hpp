#ifndef NOV_JSON_TEMPLATE_SUPPORT_SET_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_SET_HPP_

#include <set>
#include <nov/json/template/support/helpers/list_like.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<typename... Args>
class ArrayInsert<std::set<Args...> > {
public:
	typedef std::set<Args...> Type;
	typedef typename Type::value_type ValueType;

	static bool insert(Type& data, ValueType& x) {
		return data.insert(std::move(x)).second;
	}
};

template<typename... Args>
class ArrayInsert<std::multiset<Args...> > {
public:
	typedef std::multiset<Args...> Type;
	typedef typename Type::value_type ValueType;

	static bool insert(Type& data, ValueType& x) {
		data.insert(std::move(x));
		return true;
	}
};

NOV_JSON_ENABLE_ARRAY(std::set)
NOV_JSON_ENABLE_ARRAY(std::multiset)

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_SET_HPP_
