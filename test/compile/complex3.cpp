#include <string>
#include <utility>

#include <nov/json/template/support/pair.hpp>
#include <nov/json/template/support/optional.hpp>
#include <nov/json/template/create.hpp>

void compile() {
	typedef std::pair<int, boost::optional<double> > t1;
	t1 x;
	Nov::Json::Template::createHandler( x );
}

void compile1() {
	typedef std::pair<int, int> t1;
	t1 x;
	Nov::Json::Template::createHandler( x );
}
