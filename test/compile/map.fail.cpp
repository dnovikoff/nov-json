/**
 * No support/map.hpp header. Should fail - does not know how to deal with it
 */

#include <map>
#include <nov/json/template/create.hpp>

void compile() {
	std::map<std::string, int> mp;
	Nov::Json::Template::createHandler( mp );
}
