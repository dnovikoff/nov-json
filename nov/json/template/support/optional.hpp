#ifndef NOV_JSON_TEMPLATE_SUPPORT_OPTIONAL_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_OPTIONAL_HPP_

#include <boost/optional.hpp>

#include <nov/json/template/type.hpp>
#include <nov/json/template/read_array.hpp>
#include <nov/json/template/read_object.hpp>
#include <nov/json/template/write.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<typename T>
struct Type<Null, boost::optional<T> > {
	static bool jsonToUser(Null, boost::optional<T>&) {
		return true;
	}
};

template<typename JsonType, typename T>
// Inheritance is for correct convertable check
struct Type<JsonType, boost::optional<T> >: public Type<JsonType, T> {
	static bool jsonToUser(JsonType& j, boost::optional<T>& u) {
		T tmp;
		if (!Template::jsonToUser(j, tmp)) {
			return false;
		}
		u = std::move(tmp);
		return true;
	}
};


// Optional could contain containers
template<typename T>
class ReadArray<boost::optional<T> >: public ReadArray<T> {
};

template<typename T>
class ReadObject<boost::optional<T> >: public ReadObject<T> {
};

template<typename T>
struct Writer<boost::optional<T> > {
	typedef boost::optional<T> Type;

	template<typename Generator>
	static void write(Generator& g, const Type& x) {
		if (x) {
			writeValue(g, *x);
		} else {
			g.writeNull();
		}
	}
};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_OPTIONAL_HPP_
