#ifndef NOV_JSON_TEMPLATE_SUPPORT_LIST_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_LIST_HPP_

#include <list>
#include <nov/json/template/support/helpers/list_like.hpp>

#define NOV_JSON_TEMPLATE_CONTAINER_NAME std::list
#include <nov/json/template/support/helpers/list_macro.hpp>

#endif // NOV_JSON_TEMPLATE_SUPPORT_LIST_HPP_
