#include <nov/json/template/support/vector.hpp>
#include <nov/json/template/create.hpp>

class Unknown {};

void compile() {
	std::vector< Unknown > u;
	Nov::Json::Template::createHandler( u );
}
