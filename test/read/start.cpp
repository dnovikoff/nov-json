#include <test/test.hpp>

#include <nov/json/template/support/vector.hpp>

BOOST_AUTO_TEST_CASE ( startTest ) {
	std::vector<int> x2;
	BOOST_CHECK( !parseString("{}", x2) );
	BOOST_CHECK( parseString("[]", x2) );
}
