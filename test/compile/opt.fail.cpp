#include <string>
#include <utility>

#include <nov/json/template/support/tuple.hpp>
#include <nov/json/template/support/optional.hpp>
#include <nov/json/template/create.hpp>

struct Unknown {};

void compile() {
	typedef std::tuple< boost::optional<Unknown> > t1;
	t1 x;
	Nov::Json::Template::createHandler( x );
}
