#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN

#include <nov/json/template/class_container.hpp>

struct Example {
	int first;
	int second;
	std::string other;
};

namespace Nov {
namespace Json {
namespace Template {

struct t1 { NOV_JSON_META( Example, NOV_JSON_FIELD(first) ) };
struct t2 { NOV_JSON_META( Example, NOV_JSON_FIELD(first)NOV_JSON_FIELD(second) ) };
struct t3 { NOV_JSON_META( Example, NOV_JSON_FIELD(first)NOV_JSON_FIELD(second)NOV_JSON_FIELD(other) ) };

} // namespace Template
} // namespace Json
} // namespace Nov

