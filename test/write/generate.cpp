#include "test.hpp"

#include <nov/json/template/support/set.hpp>
#include <nov/json/template/support/map_as_object.hpp>
#include <nov/json/template/support/vector.hpp>
#include <nov/json/template/support/optional.hpp>
#include <nov/json/template/support/pair.hpp>
#include <nov/json/template/support/multimap.hpp>
#include <nov/json/template/support/tuple.hpp>

BOOST_AUTO_TEST_CASE ( boolean ) {
	std::vector<bool> v;
	BOOST_CHECK_EQUAL(generate(v), "[]");
	v.push_back(false);
	BOOST_CHECK_EQUAL(generate(v), "[false]");
	v.push_back(true);
	BOOST_CHECK_EQUAL(generate(v), "[false,true]");
	v.push_back(true);
	BOOST_CHECK_EQUAL(generate(v), "[false,true,true]");
}

BOOST_AUTO_TEST_CASE ( string ) {
	BOOST_CHECK_EQUAL(generateOne<std::string>("hello"), "[\"hello\"]");
}

BOOST_AUTO_TEST_CASE ( int64 ) {
	BOOST_CHECK_EQUAL(generateOne<int64_t>(1067), "[1067]");
}

BOOST_AUTO_TEST_CASE ( doublet ) {
	BOOST_CHECK_EQUAL(generateOne<double>(0.25), "[0.25]");
}

BOOST_AUTO_TEST_CASE ( intt ) {
	BOOST_CHECK_EQUAL(generateOne<int>(123), "[123]");
}

BOOST_AUTO_TEST_CASE ( pair ) {
	const auto result = generate(std::make_pair(4, true));
	BOOST_CHECK_EQUAL(result, "[4,true]");
}

BOOST_AUTO_TEST_CASE ( optional ) {
	BOOST_CHECK_EQUAL(generateOne(boost::optional<bool>()), "[null]");
	BOOST_CHECK_EQUAL(generateOne(boost::optional<bool>(true)), "[true]");
}

BOOST_AUTO_TEST_CASE ( tuple ) {
	const auto result = generate(std::make_tuple(4, true, std::string("hello")));
	BOOST_CHECK_EQUAL(result, "[4,true,\"hello\"]");
}

BOOST_AUTO_TEST_CASE ( map ) {
	std::map<std::string, int> m;
	BOOST_CHECK_EQUAL(generate(m), R"({})");
	m["one"] = 1;
	BOOST_CHECK_EQUAL(generate(m), R"({"one":1})");
	m["two"] = 2;
	BOOST_CHECK_EQUAL(generate(m), R"({"one":1,"two":2})");
	m["three"] = 3;
	// lexical order
	BOOST_CHECK_EQUAL(generate(m), R"({"one":1,"three":3,"two":2})");
}

BOOST_AUTO_TEST_CASE ( multimap ) {
	std::multimap<std::string, int> m;
	BOOST_CHECK_EQUAL(generate(m), "[]");
	m.insert(std::make_pair<std::string>("one", 1));
	BOOST_CHECK_EQUAL(generate(m), R"([["one",1]])");
	m.insert(std::make_pair<std::string>("one", 3));
	BOOST_CHECK_EQUAL(generate(m), R"([["one",1],["one",3]])");
	m.insert(std::make_pair<std::string>("two", 2));
	BOOST_CHECK_EQUAL(generate(m), R"([["one",1],["one",3],["two",2]])");
}

BOOST_AUTO_TEST_CASE ( set ) {
	std::set<int> v{1,8,3};
	BOOST_CHECK_EQUAL(generate(v), "[1,3,8]");
}
