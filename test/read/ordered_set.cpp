#include <test/test.hpp>
#include <nov/json/template/support/ordered_set.hpp>

typedef Nov::Json::OrderedSet<int> Type;

static bool testParse(const std::string& input) {
	Type tmp;
	return parseString(input, tmp);
}

BOOST_AUTO_TEST_CASE ( emptyTest ) {
	Type tmp;
	BOOST_CHECK(tmp.empty());
	BOOST_CHECK_EQUAL(tmp.size(), 0);
}

BOOST_AUTO_TEST_CASE ( insertTest ) {
	Type tmp;
	BOOST_CHECK(tmp.insert(1));
	BOOST_CHECK(!tmp.empty());
	BOOST_CHECK_EQUAL(tmp.size(), 1u);

	BOOST_CHECK(tmp.insert(2));
	BOOST_CHECK(!tmp.empty());
	BOOST_CHECK_EQUAL(tmp.size(), 2u);
}

BOOST_AUTO_TEST_CASE ( uniqueTest ) {
	Type tmp;
	BOOST_CHECK(tmp.insert(1));
	BOOST_CHECK(!tmp.insert(1));
	BOOST_CHECK_EQUAL(tmp.size(), 1u);
}

BOOST_AUTO_TEST_CASE ( parseTest ) {
	BOOST_CHECK(testParse("[]"));
	BOOST_CHECK(testParse("[1]"));
	BOOST_CHECK(testParse("[1,2]"));

	// Same key
	BOOST_CHECK(!testParse("[1,1]"));
}

BOOST_AUTO_TEST_CASE ( emptyParseTest ) {
	BOOST_CHECK(
		parseAndCheck<Type>(
			"[]",
			{}
		)
	);
}

BOOST_AUTO_TEST_CASE ( manyTest ) {
	BOOST_CHECK(
		parseAndCheck<Type>(
			"[1,2]",
			{1, 2}
		)
	);
}

BOOST_AUTO_TEST_CASE ( orderTest ) {
	BOOST_CHECK(
		parseAndCheck<Type>(
			"[2,1]",
			{2, 1}
		)
	);
}
