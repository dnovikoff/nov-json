#include <nov/json/template/create.hpp>

class Unknown {};

void compile() {
	Unknown u;
	Nov::Json::Template::createHandler( u );
}
