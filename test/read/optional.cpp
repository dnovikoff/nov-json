#include <test/test.hpp>

#include <nov/json/template/support/vector.hpp>
#include <nov/json/template/support/optional.hpp>

BOOST_AUTO_TEST_CASE ( simple ) {
	std::vector<boost::optional<int> > x;
	BOOST_REQUIRE( parseString("[1]", x) );
	BOOST_REQUIRE( x.back().is_initialized() );
	BOOST_CHECK_EQUAL( x.back(), 1);

	BOOST_REQUIRE( parseString("[null]", x) );
	BOOST_REQUIRE( !x.back().is_initialized() );

	BOOST_REQUIRE( parseString("[1,null,2]", x) );
	BOOST_CHECK( vequals(x, {1, boost::optional<int>(), 2}) );
}

BOOST_AUTO_TEST_CASE ( optionalContainer ) {
	typedef boost::optional<std::vector<int> > OptVector;
	OneArray<OptVector> x;
	BOOST_REQUIRE( parseString("[[1]]", x) );
	BOOST_REQUIRE( x->is_initialized() );
	BOOST_CHECK_EQUAL( (*x)->back(), 1u);

	BOOST_REQUIRE( parseString("[null]", x) );
	BOOST_REQUIRE( !x->is_initialized() );

	BOOST_REQUIRE( parseString("[[1,4,2]]", x) );
	BOOST_REQUIRE( x->is_initialized() );
	BOOST_CHECK( vequals(*(*x), {1, 4, 2}) );
}
