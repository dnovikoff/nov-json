#ifndef NOV_JSON_TEMPLATE_ENUM_HPP_
#define NOV_JSON_TEMPLATE_ENUM_HPP_

#include <nov/json/template/type.hpp>

namespace Nov {
namespace Json {
namespace Template {

// forward
template<typename T>
class ReadObject;

template<typename T>
class ReadArray;

// No implemetation. Meta
template<typename... Args>
class Enum;

template<typename Func, typename T, typename... Args>
struct CheckEnum;

template<typename Func, typename T>
struct CheckEnum<Func, T> {
	static constexpr bool value() {
		return false;
	}
};

template<typename Func, typename T, typename FirstArg, typename... Args>
struct CheckEnum<Func, T, FirstArg, Args...> {
	static constexpr bool value() {
		return (Func::template apply<RemoveAll<T>, RemoveAll<FirstArg> >() || CheckEnum<Func, T, Args...>::value());
	}
};

template<typename Func, typename T, typename... Args>
struct CheckEnum<Func, T, Enum<Args...> > {
	static constexpr bool value() {
		return CheckEnum<Func, T, Args...>::value();
	}
};

template<typename Func, typename T, typename X>
struct CheckEnum<Func, T, ReadObject<X> > {
	static constexpr bool value() {
		return CheckEnum<Func, T, typename ReadObject<X>::Accepted>::value();
	}
};

template<typename Func, typename T, typename X>
struct CheckEnum<Func, T, ReadArray<X> > {
	static constexpr bool value() {
		return CheckEnum<Func, T, typename ReadArray<X>::Accepted>::value();
	}
};

struct ApplyContains {
	template<typename T, typename Arg>
	static constexpr bool apply() {
		return IsSame<T, Arg>::value;
	}
};

struct ApplyConvertable {
	template<typename T, typename Arg>
	static constexpr bool apply() {
		return isConvertable<T, Arg>();
	}
};

template<typename T, typename Enum>
constexpr bool enumContaints() {
	return CheckEnum<ApplyContains, T, Enum>::value();
}

template<typename T, typename Enum>
constexpr bool enumConvartable() {
	return CheckEnum<ApplyConvertable, T, Enum>::value();
}

class NothingTag;
class ObjectTag;
class ArrayTag;

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_ENUM_HPP_
