#ifndef NOV_JSON_TEMPLATE_SUPPORT_MAP_AS_OBJECT_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_MAP_AS_OBJECT_HPP_

// !!!
#include <nov/json/template/support/helpers/map_guard.hpp>

#include <map>

#include <nov/json/template/read_object.hpp>
#include <nov/json/template/write.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<typename... Args>
class ReadObject<std::map<Args...> > {
	typedef std::map<Args...> MapType;
public:
	typedef typename MapType::mapped_type Accepted;
	typedef typename MapType::value_type PairType;
	typedef typename MapType::key_type KeyType;

	bool value(Accepted& val) {
		data_.insert(position_, PairType(std::move(key_), std::move(val)));
		return true;
	}

	template<typename T>
	bool value(T& val) {
		Accepted v;
		if (!jsonToUser(val, v)) {
			return false;
		}
		return value(v);
	}

	bool key(std::string& key) {
		position_ = data_.lower_bound(key);
		if (position_ != data_.end() && position_->first == key) {
			return false;
		}
		key_.swap(key);
		return true;
	}

	bool validate() {
		return true;
	}

	MapType move() {
		return std::move(data_);
	}

	template<typename Tag>
	std::unique_ptr<Parser::Handler> create() {
		return Detail::createFor<Accepted, Tag>([this] (Accepted&& x) {
			return value(x);
		});
	}

private:
	std::string key_;
	typename MapType::iterator position_;
	MapType data_;
};

template<typename... Args>
struct Writer<std::map<Args...> > {
	typedef std::map<Args...> MapType;

	template<typename Generator>
	static void write(Generator& g, const MapType& x) {
		g.startObject();
		for (const auto& v: x) {
			// TODO: cast for key
			writeValue(g, v.first);
			writeValue(g, v.second);
		}
		g.endObject();
	}
};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_MAP_AS_OBJECT_HPP_
