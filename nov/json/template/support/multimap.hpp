#ifndef NOV_JSON_TEMPLATE_SUPPORT_MULTIMAP_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_MULTIMAP_HPP_

#include <map>
#include <nov/json/template/support/helpers/list_like.hpp>
#include <nov/json/template/support/pair.hpp>

// Multimap is supported as Array-only (array of pairs)

namespace Nov {
namespace Json {
namespace Template {

template<typename... Args>
class ArrayInsert<std::multimap<Args...> > {
public:
	typedef std::multimap<Args...> Type;
	typedef typename PairRemoveConst<typename Type::value_type>::type ValueType;

	static bool insert(Type& data, ValueType& x) {
		data.insert(std::move(x));
		return true;
	}
};

NOV_JSON_ENABLE_ARRAY(std::multimap)

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_MULTIMAP_HPP_
