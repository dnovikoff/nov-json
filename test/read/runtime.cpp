// Check possibility of choosing the exact type on runtime

#include <test/test.hpp>
#include <nov/json/template/support/class.hpp>

struct Online {
	int id = 0;
	std::string name;
};

struct Offline {
	int id = 0;
};

struct Message {
	int id = 0;
	std::string text;
};

namespace Nov {
namespace Json {
namespace Template {

#define FIELD(NAME) if (!f(#NAME, x.NAME)) return;

template<>
struct ClassMeta<Message> {
	typedef Enum<int, std::string> Accepted;

	template<typename F>
	static void apply(Message& x, F& f) {
		FIELD(id);
		FIELD(text);
	}
};

template<>
struct ClassMeta<Online> {
	typedef Enum<int, std::string> Accepted;

	template<typename F>
	static void apply(Online& x, F& f) {
		FIELD(id);
		FIELD(name);
	}
};

template<>
struct ClassMeta<Offline> {
	typedef Enum<int> Accepted;

	template<typename F>
	static void apply(Offline& x, F& f) {
		FIELD(id);
	}
};

#undef FIELD

} // namespace Template
} // namespace Json
} // namespace Nov

using Nov::Json::Template::ObjectTag;

class CommandHandler: public Nov::Json::Parser::BaseHandler {
public:
	typedef Nov::Json::Template::Enum<std::string> Accepted;

	explicit CommandHandler(std::ostream* result): result_(result) {
	}

	CommandHandler(const CommandHandler&) = delete;
	CommandHandler& operator=(const CommandHandler&) = delete;

	CommandHandler(CommandHandler&&) = delete;
	CommandHandler& operator=( CommandHandler&&) = delete;

	std::ostream& out() {
		return *result_;
	}

	bool command(Message& x) {
		out() << "Message(" << x.id << ", '" << x.text << "')";
		return true;
	}

	bool command(Online& x) {
		out() << "Online(" << x.id << ", '" << x.name << "')";
		return true;
	}

	bool command(Offline& x) {
		out() << "Offline(" << x.id << ")";
		return true;
	}

	bool value(std::string& name) override {
		if (done_) {
			return false;
		}
		if (name == "Message") {
			handler_ = Nov::Json::Template::Detail::createFor<Message, ObjectTag>([this] (Message&& x) {
				return command(x);
			});
			return true;
		}
		if (name == "Online") {
			handler_ = Nov::Json::Template::Detail::createFor<Online, ObjectTag>([this] (Online&& x) {
				return command(x);
			});
			return true;
		}
		if (name == "Offline") {
			handler_ = Nov::Json::Template::Detail::createFor<Offline, ObjectTag>([this] (Offline&& x) {
				return command(x);
			});
			return true;
		}
		return false;
	}

	Ptr object() override {
		done_ = true;
		return std::move(handler_);
	}

	bool onParsed() override {
		return done_;
	}

private:
	std::ostream* result_ = nullptr;
	std::unique_ptr<Nov::Json::Parser::Handler> handler_;
	bool done_ = false;
};

class CommandStartHandler: public Nov::Json::Parser::BaseHandler {
public:
	CommandStartHandler(std::ostream* result): result_(result) {
	}

	Ptr array() override {
		return std::unique_ptr<CommandHandler>(new CommandHandler(result_));
	}

private:
	std::ostream* result_ = nullptr;
};

static std::string parseCommand(const std::string& input) {
	std::ostringstream oss;
	std::unique_ptr<Nov::Json::Parser::Handler> h(new CommandStartHandler(&oss));
	Nov::Json::Impl::StringParser sp;

	if (!sp.parse(input, h)) {
		return "Error: " + sp.getError();
	}
	return oss.str();
}

BOOST_AUTO_TEST_CASE ( positive ) {
	BOOST_CHECK_EQUAL( "Message(0, '')", parseCommand( R"(["Message", {}])") );
	BOOST_CHECK_EQUAL( "Message(1, 'one')", parseCommand( R"(["Message", {"id":1,"text":"one"}])") );
	BOOST_CHECK_EQUAL( "Message(2, 'two')", parseCommand( R"(["Message", {"text":"two","id":2}])") );
	BOOST_CHECK_EQUAL( "Message(0, 'zero')", parseCommand( R"(["Message", {"text":"zero"}])") );

	BOOST_CHECK_EQUAL( "Offline(6)", parseCommand( R"(["Offline", {"id":6}])") );
}

BOOST_AUTO_TEST_CASE ( negative ) {
	BOOST_CHECK_EQUAL( "Error: Unexpected key 'wrong key'", parseCommand( R"(["Offline", {"wrong key":6}])") );
	BOOST_CHECK_EQUAL( "Error: Unexpected value 'wrong type'", parseCommand( R"(["Offline", {"id":"wrong type"}])") );
}

