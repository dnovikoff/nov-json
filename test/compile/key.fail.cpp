/**
 * No conversion included for string to int key
 */

#include <nov/json/template/support/map_as_object.hpp>
#include <nov/json/template/create.hpp>

void compile() {
	std::map<int, int> mp;
	Nov::Json::Template::createHandler( mp );
}
