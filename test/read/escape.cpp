#include <test/test.hpp>

#include <nov/json/template/support/vector.hpp>

BOOST_AUTO_TEST_CASE ( stringSimpleEscapingTest ) {
	std::vector<std::string> x;

	BOOST_REQUIRE( parseString("[\"Hello\"]", x) );
	BOOST_CHECK_EQUAL( x.back(), "Hello");

	BOOST_REQUIRE( parseString(R"(["Hello\"World"])", x) );
	BOOST_CHECK_EQUAL( x.back(), R"(Hello"World)");

	BOOST_REQUIRE( parseString(R"(["Hello\\\"World"])", x) );
	BOOST_CHECK_EQUAL( x.back(), R"(Hello\"World)");

	BOOST_REQUIRE( parseString(R"(["Hello\\"])", x) );
	BOOST_CHECK_EQUAL( x.back(), R"(Hello\)");

	BOOST_REQUIRE( parseString("[\"Hello\\t\\n\\t\"]", x) );
	BOOST_CHECK_EQUAL( x.back(), "Hello\t\n\t");
}
