#include <nov/json/template/support/tuple.hpp>
#include <nov/json/template/create.hpp>

struct Unknown {};

void compile() {
	typedef std::tuple<int, double, Unknown> t;
	t x;
	Nov::Json::Template::createHandler( x );
}
