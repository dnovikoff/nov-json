#include "string_parser.hpp"
#include "internal/rapid_handler.hpp"

using namespace rapidjson;


namespace Nov {
namespace Json {
namespace Impl {

class StringParserExecute {
public:
	StringParserExecute(std::string& e):parseError(e) {
	}
	bool parse(const std::string& input, std::unique_ptr<Handler>& parser) {
		parseError.clear();
		typedef GenericReader<UTF8<>, UTF8<> > ReaderType;
		ReaderType reader;
		const unsigned int parseFlags = kParseDefaultFlags;
		typedef GenericStringStream<UTF8<> > StreamType;
		StreamType stream(input.c_str());
		RapidHandler handler;
		handler.setTop(parser);
		bool parseResult = false;
		try {
			parseResult = reader.Parse<parseFlags, GenericStringStream<UTF8<> >, RapidHandler>(stream, handler);
		} catch(const Exception& x) {
			parseError = x.what();
			return false;
		}
		if (reader.HasParseError()) {
			parseError = reader.GetParseError();
			return false;
		}
		return parseResult;
	}
private:
	std::string& parseError;
};

bool StringParser::parse(const std::string& input, std::unique_ptr<Handler>& parser) {
	StringParserExecute execute(parseError);
	return execute.parse(input, parser);
}

} // namespace Impl
} // namespace Json
} // namespace Nov
