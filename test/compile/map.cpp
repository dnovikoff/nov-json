#include <nov/json/template/support/map_as_object.hpp>
#include <nov/json/template/create.hpp>

void compile() {
	std::map<std::string, int> mp;
	Nov::Json::Template::createHandler( mp );
}
