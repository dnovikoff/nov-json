#ifndef NOV_JSON_TEMPLATE_SUPPORT_MAP_AS_ARRAY_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_MAP_AS_ARRAY_HPP_

// !!!
#include <nov/json/template/support/helpers/map_guard.hpp>

#include <map>
#include <nov/json/template/support/helpers/list_like.hpp>
#include <nov/json/template/support/pair.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<typename... Args>
class ArrayInsert<std::map<Args...> > {
public:
	typedef std::map<Args...> Type;
	typedef typename PairRemoveConst<typename Type::value_type>::type ValueType;

	static bool insert(Type& data, ValueType& x) {
		return data.insert(std::move(x)).second;
	}
};

NOV_JSON_ENABLE_ARRAY(std::map)

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_MAP_AS_ARRAY_HPP_
