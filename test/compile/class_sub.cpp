#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN

#include <nov/json/template/class_container.hpp>

struct Example {
	int first;
	int second;
	std::string other;
};

struct Example1 {
	std::string name;
	Example data;
};

NOV_JSON_CLASS_NS( Example, NOV_JSON_FIELD(first)NOV_JSON_FIELD(second)NOV_JSON_FIELD(other) )
NOV_JSON_CLASS_NS( Example1, NOV_JSON_FIELD(name)NOV_JSON_FIELD(data) )

