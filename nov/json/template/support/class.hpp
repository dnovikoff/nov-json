#ifndef NOV_JSON_TEMPLATE_SUPPORT_CLASS_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_CLASS_HPP_

#include <memory>
#include <type_traits>
#include <nov/json/handler.hpp>
#include <nov/json/template/read_object.hpp>
#include <nov/json/template/write.hpp>
#include <nov/json/template/details/pass.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<typename T>
struct ClassMeta {
	typedef NothingTag Accepted;
};

template<typename Generator>
struct WriteClass {
	template<typename T>
	inline bool operator() (const char * name, const T& value) const {
		writeValue(*generator_, name);
		writeValue(*generator_, value);
		return true;
	}

	Generator* generator_ = nullptr;
};

template<typename JsonType>
struct ReadClassField {
	template<typename T>
	inline bool operator() (const char * name, T& value) {
		if (!isConvertable<JsonType, T>() || *key_ != name) {
			return true;
		}

		result_ = jsonToUserOrFalse(*jsonValue, value);
		return false;
	}

	std::string* key_ = nullptr;
	bool result_ = false;
	JsonType* jsonValue = nullptr;
};

template<typename Tag>
struct CreateClassField {
	template<typename T>
	inline bool operator() (const char * name, T& value) {
		if (!Detail::couldBe<T, Tag>() || *key_ != name) {
			return true;
		}

		T* p = &value;
		result_ = Detail::createFor<T, Tag>([p] (T&& x) {
			return jsonToUser(x, *p);
		});
		return false;
	}

	std::string* key_ = nullptr;
	std::unique_ptr<Parser::Handler> result_;
};

struct ExistsClassField {
	template<typename T>
	inline bool operator() (const char * name, T&) {
		if (*key_ != name) {
			return true;
		}
		result_ = true;
		return false;
	}

	std::string* key_ = nullptr;
	bool result_ = false;
};


template<typename T>
class ReadClass {
public:
	typedef typename ClassMeta<T>::Accepted Accepted;

	template<typename X>
	bool value(X& val) {
		ReadClassField<X> read;
		read.key_ = &key_;
		read.jsonValue = &val;
		ClassMeta<T>::apply(data_, read);
		return read.result_;
	}

	bool key(std::string& key) {
		ExistsClassField c;
		c.key_ = &key;
		ClassMeta<T>::apply(data_, c);
		if (c.result_) {
			key_.swap(key);
			return true;
		}
		return false;
	}

	bool validate() {
		return true;
	}

	T move() {
		return std::move(data_);
	}

	template<typename Tag>
	std::unique_ptr<Parser::Handler> create() {
		CreateClassField<Tag> c;
		c.key_ = &key_;
		ClassMeta<T>::apply(data_, c);
		return std::move(c.result_);
	}

private:
	std::string key_;
	T data_;
};

template<typename T>
struct ClassWriter {
	template<typename Generator>
	static void write(Generator& g, const T& x) {
		g.startObject();
		WriteClass<Generator> writer;
		writer.generator_ = &g;
		ClassMeta<T>::apply(x, writer);
		g.endObject();
	}
};

template<typename T>
struct DefaultReadObject<T, true>: public
	std::conditional<
		std::is_same<
			typename ClassMeta<T>::Accepted,
			NothingTag
		>::value,
	DefaultReadObject<T, false>,
	ReadClass<T>
>::type
{
	typedef typename ClassMeta<T>::Accepted Accepted;
};

template<typename Generator, typename T>
struct DefaultWriter<Generator, T, true> {
	static void write(Generator& g, const T& x) {
		typedef typename std::conditional<std::is_same<NothingTag, typename ClassMeta<T>::Accepted >::value, Writer<T>, ClassWriter<T> >::type WriterType;
		WriterType::write(g, x);
	}
};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_CLASS_HPP_
