#ifndef NOV_JSON_TEMPLATE_DETAILS_HANDLER_HPP_
#define NOV_JSON_TEMPLATE_DETAILS_HANDLER_HPP_

#include <functional>

#include <nov/json/handler.hpp>
#include <nov/json/template/enum.hpp>
#include <nov/json/template/read_array.hpp>
#include <nov/json/template/read_object.hpp>
#include <nov/json/template/details/pass.hpp>

namespace Nov {
namespace Json {
namespace Template {
namespace Detail {

template<typename T, typename Tag, typename Callback>
class ReadHandler: public Parser::Handler {
public:
	explicit ReadHandler(const Callback& cb): callback_(cb) {
	}

	bool value(Parser::Integer x) override {
		return pass(read_, x);
	}
	bool value(bool x) override {
		return pass(read_, x);
	}
	bool value(double x) override {
		return pass(read_, x);
	}
	bool value(std::string& x) override {
		return pass(read_, x);
	}
	bool null() override {
		const Null null;
		return pass(read_, null);
	}
	bool key(std::string& x) override {
		return passKey(read_, x);
	}
	Ptr object() override {
		return read_.template create<ObjectTag>();
	}
	Ptr array() override {
		return read_.template create<ArrayTag>();
	}
	bool onParsed() override {
		const bool r = read_.validate() && callback_(read_.move());
		return r;
	}

private:
	Callback callback_;
	typename Detail::ReadFor<T, Tag>::type read_;
};

} // namespace Detail
} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_DETAILS_HANDLER_HPP_
