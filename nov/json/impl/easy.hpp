#ifndef NOV_JSON_IMPL_EASY_HPP_
#define NOV_JSON_IMPL_EASY_HPP_

#include <stdexcept>

#include <nov/json/impl/string_parser.hpp>
#include <nov/json/template/create.hpp>

namespace Nov {
namespace Json {
namespace Impl {

template<typename T>
inline void easyParseSkippingComments(const std::string& input, T& data, const std::string& commentKey="//") {
	Nov::Json::Parser::Handler::Ptr h = Nov::Json::Template::createHandler(data);
	Nov::Json::Impl::StringParser sp;

	if (!sp.parseSkippingComments(input, h, commentKey)) {
		throw std::runtime_error("EasyParse error: " + sp.getError());
	}
}

template<typename T>
inline void easyParse(const std::string& input, T& data) {
	Nov::Json::Parser::Handler::Ptr h = Nov::Json::Template::createHandler(data);
	Nov::Json::Impl::StringParser sp;

	if (!sp.parse(input, h)) {
		throw std::runtime_error("EasyParse error: " + sp.getError());
	}
}

} // namespace Impl
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_IMPL_EASY_HPP_
