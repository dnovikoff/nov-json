#include <test/test.hpp>
#include <nov/json/template/support/map_as_object.hpp>

BOOST_AUTO_TEST_CASE ( mapTest ) {
	std::map<std::string, int> x;
	BOOST_CHECK( parseString("{}", x) );
	BOOST_CHECK( x.empty() );
	BOOST_CHECK( parseString(R"({"hello":1})", x) );
	BOOST_CHECK_EQUAL( x.size(), 1u );
	BOOST_CHECK_EQUAL(x["hello"], 1);
	BOOST_CHECK( parseString(R"({"hello":1, "world":5})", x) );
	BOOST_CHECK_EQUAL( x.size(), 2u );
	BOOST_CHECK_EQUAL(x["hello"], 1);
	BOOST_CHECK_EQUAL(x["world"], 5);
}

BOOST_AUTO_TEST_CASE ( typeTest ) {
	std::map<std::string, int> x;
	BOOST_CHECK( !parseString("[]", x) );
	BOOST_CHECK( parseString("{}", x) );
}
