#ifndef NOV_JSON_TEMPLATE_KEY_HPP_
#define NOV_JSON_TEMPLATE_KEY_HPP_

#include <string>

namespace Nov {
namespace Json {
namespace Template {

/**
 * This class describes how string key could be converted to other types
 */
template<typename UserType>
struct Key;

// Nothing to do for string
template<>
struct Key<std::string> {
	static bool convert(std::string& parsed, std::string& user) {
		user.swap(parsed);
		return true;
	}
};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_KEY_HPP_
