#include <string>
#include <utility>

#include <nov/json/template/support/tuple.hpp>
#include <nov/json/template/create.hpp>

void compile() {
	typedef std::tuple< std::string > t1;
	t1 x;
	Nov::Json::Template::createHandler( x );
}
