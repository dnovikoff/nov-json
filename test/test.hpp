#ifndef TEST_TEST_HPP_
#define TEST_TEST_HPP_

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN

#include <iostream>

#include <boost/test/unit_test.hpp>

#include <nov/json/impl/string_parser.hpp>

#include <nov/json/template/support/vector.hpp>

#include <boost/range.hpp>

#include <test/parse_string.hpp>
#include <test/one_array.hpp>

template<typename T1, typename T2>
std::ostream& operator<<(std::ostream& os, const std::pair<T1, T2>& p) {
	os << "pair[" << p.first << ", " << p.second << "]";
	return os;
}

template<typename R>
static void dumpRange(const R& x1, const std::string& label) {
	auto x1b = std::begin(x1);
	auto x1e = std::end(x1);

	std::cout << "Dumping range " << label << std::endl;
	std::cout << "Size = " << std::distance(x1b, x1e) << std::endl;
	for(const auto& v:x1) {
		std::cout << v << std::endl;
	}
	std::cout << std::endl;
}

struct Compare {
	template<typename T>
	bool operator()(const T& x1, const T& x2) {
		return x1 == x2;
	}

	bool operator()(double x1, double x2) {
		return std::abs(x1-x2) < 0.000001;
	}
};

template<typename R1, typename R2>
static bool cequals(const R1& x1, const R2& x2) {
	auto x1b = std::begin(x1);
	auto x1e = std::end(x1);

	auto x2b = std::begin(x2);
	auto x2e = std::end(x2);

	auto d1 = std::distance(x1b, x1e);
	auto d2 = std::distance(x2b, x2e);

	if( d1 != d2 || !std::equal(x1b, x1e, x2b, Compare()) ) {
		dumpRange(x1, "left");
		dumpRange(x2, "right");
		return false;
	}
	return true;
}

template<typename C>
static bool vequals(const C& x1, const std::vector<typename C::value_type>& x2) {
	return cequals(x1, x2);
}

template<typename T, typename Y>
const T& getOne(const std::vector<Y>& x) {
	assert( x.size() == 1u);
	return boost::get<T>( x.back() );
}

template<typename C>
static bool parseAndCheck(const std::string& input, const std::vector<typename C::value_type>& ethalon) {
	C data;
	if (!parseString(input, data)) return false;
	return vequals(data, ethalon);
}

#endif // TEST_TEST_HPP_
