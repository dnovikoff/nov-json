#ifndef NOV_JSON_TEMPLATE_HPP_
#define NOV_JSON_TEMPLATE_HPP_

/**
 * Includes all helpers for all containers and types
 */

#include <nov/json/template/create.hpp>
#include <nov/json/template/support/all.hpp>

#endif // NOV_JSON_TEMPLATE_HPP_
