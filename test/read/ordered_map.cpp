#include <test/test.hpp>
#include <nov/json/template/support/ordered_map.hpp>

typedef Nov::Json::OrderedMap<std::string, int> Type;

static bool testParse(const std::string& input) {
	Type tmp;
	return parseString(input, tmp);
}

BOOST_AUTO_TEST_CASE ( emptyTest ) {
	Type tmp;
	BOOST_CHECK(tmp.empty());
	BOOST_CHECK_EQUAL(tmp.size(), 0);
}

BOOST_AUTO_TEST_CASE ( insertTest ) {
	Type tmp;
	BOOST_CHECK(tmp.insert(Type::PairType("first",1)));
	BOOST_CHECK(!tmp.empty());
	BOOST_CHECK_EQUAL(tmp.size(), 1u);

	BOOST_CHECK(tmp.insert(Type::PairType("second",1)));
	BOOST_CHECK(!tmp.empty());
	BOOST_CHECK_EQUAL(tmp.size(), 2u);
}

BOOST_AUTO_TEST_CASE ( uniqueTest ) {
	Type tmp;
	BOOST_CHECK(tmp.insert(Type::PairType("first",1)));
	BOOST_CHECK(!tmp.insert(Type::PairType("first",1)));
	BOOST_CHECK_EQUAL(tmp.size(), 1u);
}

BOOST_AUTO_TEST_CASE ( parseTest ) {
	BOOST_CHECK(testParse("{}"));
	BOOST_CHECK(testParse(R"({"one": 1})"));
	BOOST_CHECK(testParse(R"({"one": 1, "two": 2})"));

	// Same key
	BOOST_CHECK(!testParse(R"({"one": 1, "one": 2})"));
}

BOOST_AUTO_TEST_CASE ( emptyParseTest ) {
	BOOST_CHECK(
		parseAndCheck<Type>(
			"{}",
			{}
		)
	);
}

BOOST_AUTO_TEST_CASE ( manyTest ) {
	BOOST_CHECK(
		parseAndCheck<Type>(
			R"({"first": 1, "second": 2})",
			{{"first",1}, {"second",2}}
		)
	);
}

BOOST_AUTO_TEST_CASE ( orderTest ) {
	BOOST_CHECK(
		parseAndCheck<Type>(
			R"({"second": 1, "first": 2})",
			{{"second",1}, {"first",2}}
		)
	);
}

BOOST_AUTO_TEST_CASE ( operatorTest ) {
	Type t;
	t["hello"] = 1;
	t["world"] = 4;
	BOOST_CHECK(vequals(t, {{"hello", 1},{"world",4}}));
	const Type& tConst = t;
	BOOST_CHECK_EQUAL(tConst["hello"], 1);
	BOOST_CHECK_EQUAL(tConst["world"], 4);

	BOOST_CHECK(tConst.contains("hello"));
	BOOST_CHECK(!tConst.contains("not found"));

	BOOST_CHECK_THROW(tConst["not found"], std::out_of_range);
}

BOOST_AUTO_TEST_CASE ( eraseTest ) {
	Type t;
	t["1"] = 1;
	BOOST_CHECK(!t.empty());
	BOOST_CHECK_EQUAL(t.size(), 1u);
	t.erase("1");
	BOOST_CHECK(t.empty());
	BOOST_CHECK_EQUAL(t.size(), 0u);
}
