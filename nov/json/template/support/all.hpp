#ifndef NOV_JSON_TEMPLATE_SUPPORT_ALL_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_ALL_HPP_

/**
 * Support all default types with one include
 */

// arrays
//#include <nov/json/template/support/list.hpp>
#include <nov/json/template/support/vector.hpp>
//#include <nov/json/template/support/deque.hpp>
//#include <nov/json/template/support/set.hpp>
#include <nov/json/template/support/pair.hpp>
//#include <nov/json/template/support/tuple.hpp>
//#include <nov/json/template/support/multimap.hpp>

// Object
//#include <nov/json/template/support/map_as_object.hpp>

// types
//#include <nov/json/template/support/optional.hpp>
//#include <nov/json/template/support/variant.hpp>

#endif // NOV_JSON_TEMPLATE_SUPPORT_ALL_HPP_
