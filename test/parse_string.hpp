#ifndef TEST_PARSE_STRING_HPP_
#define TEST_PARSE_STRING_HPP_

#include <nov/json/impl/string_parser.hpp>
#include <nov/json/template/create.hpp>

template<typename T>
static bool parseString(const std::string& input, T& data) {
	Nov::Json::Parser::Handler::Ptr h = Nov::Json::Template::createHandler(data);
	Nov::Json::Impl::StringParser sp;

	if (!sp.parse(input, h)) {
		std::cout << "NOTE error: " << sp.getError() << std::endl;
		return false;
	}
	return true;
}

#endif // TEST_PARSE_STRING_HPP_
