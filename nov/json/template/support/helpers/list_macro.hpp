#include <nov/json/template/support/helpers/list_like.hpp>

namespace Nov {
namespace Json {
namespace Template {

NOV_JSON_ENABLE_ARRAY(NOV_JSON_TEMPLATE_CONTAINER_NAME)

} // namespace Template
} // namespace Json
} // namespace Nov

#undef NOV_JSON_TEMPLATE_CONTAINER_NAME
