#ifndef TEST_ONE_ARRAY_HPP_
#define TEST_ONE_ARRAY_HPP_

#include <boost/variant/get.hpp>
#include <nov/json/template/read_array.hpp>

/**
 * Special testing helpers class
 */
template<typename T>
class OneArray {
public:
	T value;
	T& operator*() {
		return value;
	}
	T* operator->() {
		return &value;
	}
	// for variant fast get

	template<typename Y>
	Y& get() {
		return boost::get<Y>( value );
	}
};

namespace Nov {
namespace Json {
namespace Template {

/**
 * Helper class for easy testing
 * Still is shows how to write specialization for your own container
 * This container allows arrays of only one value
 */
template<typename T>
class ReadArray<OneArray<T> > {
public:
	typedef T Accepted;

	template<typename X>
	bool value(X& x) {
		if (inserted_) {
			return false;
		}
		inserted_ = true;
		return jsonToUser(x, data_.value);
	}

	bool value(Accepted& x) {
		if (inserted_) {
			return false;
		}
		inserted_ = true;
		data_.value = std::move(x);
		return true;
	}

	bool validate() {
		return inserted_;
	}

	template<typename Tag>
	std::unique_ptr<Parser::Handler> create() {
		if (inserted_) {
			return nullptr;
		}
		return Detail::createFor<T, Tag>([this] (T&& x) {
			return value(x);
		});
	}

	OneArray<T> move() {
		return std::move(data_);
	}

private:
	bool inserted_ = false;
	OneArray<T> data_;
};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // TEST_ONE_ARRAY_HPP_
