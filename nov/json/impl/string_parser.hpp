#ifndef NOV_JSON_IMPL_STRING_PARSER_HPP_
#define NOV_JSON_IMPL_STRING_PARSER_HPP_

#include <memory>
#include <string>

namespace Nov {
namespace Json {

namespace Parser {
class Handler;
} // namespace Parser

namespace Impl {

typedef Parser::Handler Handler;

class StringParser {
	std::string parseError;
public:
	bool parse(const std::string& input, std::unique_ptr<Handler>& parser);

	// Just the same as parse, but to be used as functor
	inline bool operator()(const std::string& input, std::unique_ptr<Handler>& parser ) {
		return parse(input, parser);
	}
	const std::string& getError() const {
		return parseError;
	}
};

} // namespace Impl
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_IMPL_STRING_PARSER_HPP_
