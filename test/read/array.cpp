#include <test/test.hpp>

#include <nov/json/template/support/vector.hpp>
#include <nov/json/template/support/list.hpp>
#include <nov/json/template/support/deque.hpp>
#include <nov/json/template/support/set.hpp>


BOOST_AUTO_TEST_CASE ( vectorTest ) {
	std::vector<int> x;
	BOOST_CHECK( parseString("[]", x) );
	BOOST_CHECK( x.empty() );

	BOOST_CHECK( parseString("[721]", x) );
	BOOST_REQUIRE_EQUAL( x.size(), 1u );
	BOOST_CHECK_EQUAL(x.back(), 721);

	// old data should be removed
	BOOST_CHECK( parseString("[123]", x) );
	BOOST_REQUIRE_EQUAL( x.size(), 1u );
	BOOST_CHECK_EQUAL(x.back(), 123);

	BOOST_CHECK( parseString("[123, 321, 0, 4, 12]", x) );

	BOOST_CHECK( vequals(x, {123, 321, 0, 4, 12}) );
}

BOOST_AUTO_TEST_CASE ( listTest ) {
	std::list<int> x;
	BOOST_CHECK( parseString("[]", x) );
	BOOST_CHECK( x.empty() );

	BOOST_CHECK( parseString("[721]", x) );
	BOOST_REQUIRE_EQUAL( x.size(), 1u );
	BOOST_CHECK_EQUAL(x.back(), 721);

	// old data should be removed
	BOOST_CHECK( parseString("[123]", x) );
	BOOST_REQUIRE_EQUAL( x.size(), 1u );
	BOOST_CHECK_EQUAL(x.back(), 123);

	parseString("[123, 321, 0, 4, 12]", x);
	BOOST_CHECK( vequals(x, {123, 321, 0, 4, 12}) );
}

BOOST_AUTO_TEST_CASE ( setTest ) {
	std::set<int> x;
	BOOST_CHECK( parseString("[]", x) );
	BOOST_CHECK( x.empty() );

	BOOST_CHECK( parseString("[721]", x) );
	BOOST_REQUIRE_EQUAL( x.size(), 1u );
	BOOST_CHECK_EQUAL(*x.begin(), 721);

	// old data should be removed
	BOOST_CHECK( parseString("[123]", x) );
	BOOST_REQUIRE_EQUAL( x.size(), 1u );
	BOOST_CHECK_EQUAL(*x.begin(), 123);

	parseString("[123, 321, 0, 4, 12]", x);

	BOOST_CHECK( vequals(x, {0, 4, 12, 123, 321}) );

	// multiple same values
	BOOST_CHECK( !parseString("[1,4,1]", x) );

	//just compilation test
	std::set<int, std::greater<int> > x2;
	BOOST_CHECK( parseString("[]", x2) );
}

BOOST_AUTO_TEST_CASE ( multisetTest ) {
	BOOST_CHECK(
		parseAndCheck<std::multiset<int> >(
			"[123, 321, 0, 4, 12, 0, 0, 123]",
			{0, 0, 0, 4, 12, 123, 123, 321}
		)
	);

	typedef std::multiset<int, std::greater<int> > MultsetGreater;
	BOOST_CHECK(
		parseAndCheck<MultsetGreater>(
			"[123, 321, 0, 4, 12, 0, 0, 123]",
			{321, 123, 123, 12, 4, 0, 0, 0}
		)
	);
}

BOOST_AUTO_TEST_CASE ( dequeTest ) {
	std::deque<int> x;
	BOOST_CHECK( parseString("[123, 321, 0, 4, 12, 0, 0, 123]", x) );
	BOOST_REQUIRE_EQUAL(x.size(), 8u);
	BOOST_REQUIRE_EQUAL(x.back(), 123);
}

