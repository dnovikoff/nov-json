#include "string_generator.hpp"

#include <ostream>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

namespace Nov {
namespace Json {
namespace Impl {

typedef rapidjson::Writer<rapidjson::StringBuffer> Writer;

struct Implementation {
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer;

	Implementation():writer(buffer) {
	}

	Implementation(const Implementation&) = delete;
	Implementation& operator=(const Implementation&) = delete;
};

static Implementation* cast(void* x) {
	return static_cast<Implementation*>(x);
}

#define WRITER cast(this->impl.get())->writer

void StringGenerator::ImplDeleter::operator()(void* x) {
	delete cast(x);
}

StringGenerator::StringGenerator():impl(new Implementation) {
}

void StringGenerator::startObject() {
	WRITER.StartObject();
}

void StringGenerator::endObject() {
	WRITER.EndObject();
}

void StringGenerator::startArray() {
	WRITER.StartArray();
}

void StringGenerator::endArray() {
	WRITER.EndArray();
}

void StringGenerator::write(const char* x) {
	WRITER.String(x);
}

void StringGenerator::write(const std::string& x) {
	WRITER.String(x.c_str(), x.size());
}

void StringGenerator::write(const int64_t x) {
	WRITER.Int64(x);
}

void StringGenerator::write(const double x) {
	WRITER.Double(x);
}

void StringGenerator::write(const bool x) {
	WRITER.Bool(x);
}

void StringGenerator::writeNull() {
	WRITER.Null();
}

std::string StringGenerator::result() const {
	const rapidjson::StringBuffer& buffer = cast(impl.get())->buffer;
	return std::string(buffer.GetString(), buffer.GetSize());
}

} // namespace Impl
} // namespace Json
} // namespace Nov
