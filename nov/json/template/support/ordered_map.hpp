#ifndef NOV_JSON_TEMPLATE_SUPPORT_ORDERED_MAP_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_ORDERED_MAP_HPP_

#include <list>
#include <string>
#include <map>
#include <stdexcept>

#include <nov/json/template/read_object.hpp>
#include <nov/json/template/write.hpp>

#include <nov/json/template/support/ordered_base.hpp>

namespace Nov {
namespace Json {

struct OrderedMapKeyExtractPolicy {
	template<typename K, typename V>
	const K& operator()(const std::pair<K, V>& x) {
		return x.first;
	}
};

// Special class to read maps with correct order
template<typename Key, typename Value>
class OrderedMap: public OrderedBase<Key, std::pair<Key, Value>, OrderedMapKeyExtractPolicy> {
public:
	typedef std::pair<Key, Value> PairType;
	typedef OrderedBase<Key, PairType, OrderedMapKeyExtractPolicy> ParentType;

	const Value& operator[](const Key& key) const {
		auto i = ParentType::find(key);
		if (i == ParentType::end()) {
			throw std::out_of_range("Key out of range");
		}
		return i->second;
	}

	Value& operator[](const Key& key) {
		auto i = ParentType::find(key);
		if (i == ParentType::end()) {
			ParentType::insert(PairType(key, (Value())));
			return ParentType::back().second;
		}
		return i->second;
	}

	OrderedMap() {
	}

	template<typename Iter>
	OrderedMap(Iter begin, Iter end) {
		for (; begin != end; ++begin) {
			insert(*begin);
		}
	}

	OrderedMap(std::initializer_list<std::pair<Key, Value> > l):OrderedMap(l.begin(), l.end()) {
	}
};

namespace Template {

template<typename Value>
class ReadObject<OrderedMap<std::string, Value> > {
	typedef OrderedMap<std::string, Value> MapType;
	typedef typename MapType::PairType PairType;
public:
	typedef std::string KeyType;
	typedef Value ValueType;
	typedef ValueType Accepted;

	bool value(ValueType& val) {
		return data_.insert(PairType((std::move(key_)), (std::move(val))));
	}

	template<typename X>
	bool value(X& val) {
		ValueType userValue;
		if (!jsonToUser(val, userValue)) {
			return false;
		}
		return value(userValue);
	}

	bool key(std::string& x) {
		if (data_.contains(x)) {
			return false;
		}
		key_ = std::move(x);
		return true;
	}

	template<typename Tag>
	std::unique_ptr<Parser::Handler> create() {
		return Detail::createFor<ValueType, Tag>([this] (ValueType&& x) {
			return value(x);
		});
	}

	bool validate() {
		return true;
	}

	MapType move() {
		return std::move(data_);
	}

private:
	std::string key_;
	MapType data_;
};

template<typename... Args>
struct Writer<OrderedMap<Args...> > {
	typedef OrderedMap<Args...> MapType;

	template<typename Generator>
	static void write(Generator& g, const MapType& x) {
		g.startObject();
		for (const auto& v: x) {
			writeValue(g, v.first);
			writeValue(g, v.second);
		}
		g.endObject();
	}
};

} // namespace Template

} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_ORDERED_MAP_HPP_
