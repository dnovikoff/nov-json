// This header included in map_as_array.hpp and map_as_object.hpp
// preventing them to be included at one time

#ifdef NOV_JSON_MAP_INCLUDED
#error "Two different support implementations for map could not be included at the same time"
#endif

#define NOV_JSON_MAP_INCLUDED
