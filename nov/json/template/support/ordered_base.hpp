#ifndef NOV_JSON_TEMPLATE_SUPPORT_ORDERED_BASE_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_ORDERED_BASE_HPP_

#include <list>
#include <map>
#include <stdexcept>

namespace Nov {
namespace Json {

template<typename Key, typename Data, typename KeyExtractPolicy>
class OrderedBase {
	struct Comparator {
		inline bool operator()(const Key* const lhs, const Key* const rhs) const {
			return *lhs < *rhs;
		}
	};
private:
	inline static const Key& extractKey(const Data& d) {
		KeyExtractPolicy e;
		return e(d);
	}
public:
	typedef Data value_type;

	typedef std::list<value_type> DataType;
	typedef typename DataType::const_iterator Iterator;

	typedef std::map<const Key*, typename DataType::iterator, Comparator> MappingType;
	typedef typename MappingType::value_type MappingTypePair;

	bool contains(const Key& key) const {
		return mapping.find(&key) != mapping.end();
	}

	bool erase(const Key& key) {
		auto i = mapping.find(&key);
		if (i == mapping.end()) {
			return false;
		}
		data.erase(i->second);
		mapping.erase(i);
		return true;
	}

	bool insert(Data&& record) {
		const auto& key = extractKey(record);
		auto i = mapping.lower_bound(&key);
		if (i != mapping.end() && *(i->first) == key) {
			return false;
		}
		auto iter = data.insert(data.end(), std::move(record));
		mapping.insert(MappingTypePair(&extractKey(*iter), iter));
		return true;
	}

	Iterator find(const Key& key) const {
		auto i = mapping.find(&key);
		if (i == mapping.end()) {
			return data.end();
		}
		return i->second;
	}

	Iterator begin() const {
		return data.begin();
	}
	Iterator end() const {
		return data.end();
	}
	bool empty() const {
		return data.empty();
	}
	size_t size() const {
		return mapping.size();
	}
	const Data& back() const {
		return data.back();
	}
	virtual ~OrderedBase() {
	}
protected:
	Data& back() {
		return data.back();
	}
	typename DataType::iterator find(const Key& key) {
		auto i = mapping.find(&key);
		if (i == mapping.end()) {
			return data.end();
		}
		return i->second;
	}
private:
	DataType data;
	MappingType mapping;
};

} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_ORDERED_BASE_HPP_
