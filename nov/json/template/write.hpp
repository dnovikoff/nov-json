#ifndef NOV_JSON_TEMPLATE_WRITE_HPP_
#define NOV_JSON_TEMPLATE_WRITE_HPP_

#include <string>

namespace Nov {
namespace Json {
namespace Template {

template<bool castToInt64>
struct WriterDetail {
	template<typename T, typename Generator>
	static void write(Generator& g, const T& x) {
		g.write(x);
	}
};

template<>
struct WriterDetail<true> {
	template<typename T, typename Generator>
	static void write(Generator& g, const T& x) {
		g.write(static_cast<int64_t>(x));
	}
};

template<typename T>
struct Writer {
	static const bool castToInt64 = std::is_integral<T>::value && !std::is_same<T, bool>::value && !std::is_same<T, int64_t>::value;

	template<typename Generator>
	static void write(Generator& g, const T& x) {
		WriterDetail<castToInt64>::write(g, x);
	}
};


template<typename Generator, typename T, bool>
struct DefaultWriter {
	static void write(Generator& g, const T& x) {
		Writer<T>::write(g, x);
	}
};

template<typename Generator, typename T>
static void writeValue(Generator& g, const T& x) {
	DefaultWriter<Generator, T, true>::write(g, x);
}

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_WRITE_HPP_
