#ifndef NOV_JSON_TEMPLATE_SUPPORT_HELPERS_LIST_LIKE_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_HELPERS_LIST_LIKE_HPP_

#include <nov/json/template/read_array.hpp>
#include <nov/json/template/write.hpp>
#include <nov/json/template/details/pass.hpp>
#include <nov/json/template/type.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<typename T>
class ArrayInsert {
public:
	typedef typename T::value_type ValueType;
	static bool insert(T& data, ValueType& x) {
		data.push_back(std::move(x));
		return true;
	}
};

template<typename T>
class ReadArrayContainer {
public:
	typedef typename ArrayInsert<T>::ValueType Accepted;

	template<typename X>
	bool value(X& x) {
		Accepted a;
		if (!jsonToUser(x, a)) {
			return false;
		}
		return ArrayInsert<T>::insert(data_, a);
	}
	bool value(Accepted& x) {
		return ArrayInsert<T>::insert(data_, x);
	}
	bool validate() {
		return true;
	}
	template<typename Tag>
	std::unique_ptr<Parser::Handler> create() {
		return Detail::createFor<Accepted, Tag>([this] (Accepted&& x) {
			return value(x);
		});
	}

	T move() {
		return std::move(data_);
	}

private:
	T data_;
};

template<typename T>
class ArrayWriter {
public:
	template<typename Generator>
	static void write(Generator& g, const T& x) {
		g.startArray();
		for (const auto& v: x) {
			writeValue(g, v);
		}
		g.endArray();
	}

	// Just warning issues
	virtual ~ArrayWriter() {
	}
};

#define NOV_JSON_ENABLE_ARRAY(NAME)\
template<typename... Args>\
class ReadArray<NAME<Args...> >: public ReadArrayContainer<NAME<Args...> > {};\
template<typename... Args>\
struct Writer<NAME<Args...> >: public ArrayWriter<NAME<Args...> > {};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_HELPERS_LIST_LIKE_HPP_
