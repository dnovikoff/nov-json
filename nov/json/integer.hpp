#ifndef NOV_JSON_INTEGER_HPP_
#define NOV_JSON_INTEGER_HPP_

#include <cstdint>
#include <type_traits>
#include <limits>

namespace Nov {
namespace Json {
namespace Parser {

/**
 * Special class to simplify work with integer and to hide its real type ( to be possible changed )
 */
class Integer {
	template<typename T>
	struct Supports {
		typedef typename std::remove_cv<T>::type NoCv;
		typedef typename std::remove_reference<NoCv>::type Clean;

		static const bool isBool = std::is_same<Clean, bool>::value;
		static const bool isFloat = std::is_same<Clean, float>::value || std::is_same<Clean, double>::value;
		static const bool isIntegral = std::is_integral<Clean>::value;

		static constexpr bool value() {
			return !isBool && (isFloat || isIntegral);
		}
	};
public:
	explicit Integer(int64_t v): data_(v) {
	}

	template<typename T>
	static constexpr bool supports() {
		return Supports<T>::value();
	}

	template<typename T>
	bool to(T& integral) const {
		static const DataType originalMax = std::numeric_limits<T>::max();

		// Does not accept top uint64 values
		static const DataType maxValue = (originalMax!=-1)? originalMax: std::numeric_limits<DataType>::max();
		static const DataType minValue = std::numeric_limits<T>::min();

		if( data_ < minValue || data_ > maxValue ) {
			return false;
		}
		integral = static_cast<T>(data_);
		return true;
	}

	bool to(double& d) const {
		d = static_cast<double>(data_);
		return true;
	}

	bool to(float& d) const {
		d = static_cast<float>(data_);
		return true;
	}

private:
	typedef int64_t DataType;
	DataType data_;
};

} // namespace Parser
} // namespace Json
} // namsspace Nov

#endif // NOV_JSON_INTEGER_HPP_
