#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN

#include <iostream>

#include <boost/test/unit_test.hpp>

#include <nov/json/impl/string_parser.hpp>
#include <nov/json/template/create.hpp>
#include <nov/json/template/class_container.hpp>

#include "one_array.hpp"
#include "parse_string.hpp"

struct Example {
	int first;
	int second;
	std::string other;
};

NOV_JSON_CLASS_NS( Example , NOV_JSON_FIELD(first)NOV_JSON_FIELD(second)NOV_JSON_FIELD(other) )

BOOST_AUTO_TEST_CASE ( simpleTest ) {
	Example e;
	BOOST_CHECK( !parseString("{}", e) );

	BOOST_REQUIRE( parseString( R"({"first":345, "second" : 999, "other": "Dmitri Novikov"})", e) );
	BOOST_CHECK_EQUAL( e.first, 345 );
	BOOST_CHECK_EQUAL( e.second, 999 );
	BOOST_CHECK_EQUAL( e.other, "Dmitri Novikov" );

	// one more field
	BOOST_REQUIRE( !parseString( R"({"first":345, "second" : 999, "other": "Dmitri Novikov", "onmorefield": 0})", e) );

	// wrong type
	BOOST_REQUIRE( !parseString( R"({"first": "d", "second" : 999, "other": "Dmitri Novikov"})", e) );

	// one field less
	BOOST_REQUIRE( !parseString( R"({"first":345, "second" : 999})", e) );
}

BOOST_AUTO_TEST_CASE ( complexTest ) {
	OneArray<Example> e;
	BOOST_CHECK( !parseString("[{}]", e) );

	BOOST_REQUIRE( parseString( R"([{"first":345, "second" : 999, "other": "Dmitri Novikov"}])", e) );
	BOOST_CHECK_EQUAL( e->first, 345 );
	BOOST_CHECK_EQUAL( e->second, 999 );
	BOOST_CHECK_EQUAL( e->other, "Dmitri Novikov" );
}

// Subtype
struct Example1 {
	std::string name;
	Example data;
};

NOV_JSON_CLASS_NS( Example1 , NOV_JSON_FIELD(name)NOV_JSON_FIELD(data) )

BOOST_AUTO_TEST_CASE ( subClassTest ) {
	Example1 e;
	BOOST_REQUIRE( parseString( R"({"name":"MyObject", "data":{"first":345, "second" : 999, "other": "Dmitri Novikov"}})", e) );
	BOOST_CHECK_EQUAL( e.name, "MyObject" );
	BOOST_CHECK_EQUAL( e.data.first, 345 );
	BOOST_CHECK_EQUAL( e.data.second, 999 );
	BOOST_CHECK_EQUAL( e.data.other, "Dmitri Novikov" );
}

