#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN

#include <nov/json/template/create.hpp>
#include <nov/json/template/class_container.hpp>
#include <nov/json/template/support/list.hpp>

struct Example {
	int first;
	int second;
	std::string other;
};

NOV_JSON_CLASS_NS( Example , NOV_JSON_FIELD(first)NOV_JSON_FIELD(second)NOV_JSON_FIELD(other) )

void compile() {
	Example x;
	Nov::Json::Template::createHandler( x );
}

void compile1() {
	std::list<Example> x;
	Nov::Json::Template::createHandler( x );
}
