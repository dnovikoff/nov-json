#ifndef NOV_JSON_TEMPLATE_DETAILS_PASS_HPP_
#define NOV_JSON_TEMPLATE_DETAILS_PASS_HPP_

#include <nov/json/template/enum.hpp>

namespace Nov {
namespace Json {
namespace Template {
namespace Detail {

template<typename T, typename Tag>
struct ReadFor {
	typedef ReadObject<NothingTag> type;
};

template<typename T>
struct ReadFor<T, ObjectTag> {
	typedef ReadObject<T> type;
};

template<typename T>
struct ReadFor<T, ArrayTag> {
	typedef ReadArray<T> type;
};

template<typename T, typename Tag>
struct AcceptedFor {
	typedef typename ReadFor<T, Tag>::type ForType;
	typedef typename ForType::Accepted type;
};


template<typename T, typename Tag>
constexpr bool couldBe() {
	return !IsSame<typename AcceptedFor<T, Tag>::type, NothingTag>::value;
}

template<typename T, typename Tag, typename Callback>
class ReadHandler;

template<typename T, bool enabled =
	couldBe<T, ObjectTag>() || couldBe<T, ArrayTag>()
	|| isConvertable<std::string, T>()
	|| isConvertable<Null, T>()
	|| isConvertable<double, T>()
	|| isConvertable<bool, T>()
	|| isConvertable<Parser::Integer, T>()
>
struct StaticDebug {
	static constexpr bool value() {
		return false;
	}
};

template<typename T>
struct StaticDebug<T, true> {
	static constexpr bool value() {
		return true;
	}

	typedef T type;
};

template<typename T>
struct ValidateType {
	// We should check the possibility
	// of existing this type
	typedef StaticDebug<T> Debug;
	static_assert(
			Debug::value(), "This type have no specialization for Type, ReadArray or ReadObject. Did you forget to include corresponding type support? See DebugInfo for details");
	// Show debug info in compile time error
	typedef typename Debug::type DebugInfo;
};

template<typename T>
using Validate = typename ValidateType<T>::DebugInfo;

template<typename Read, typename T, bool=enumConvartable<T, Read>()>
struct PassIf {
	typedef Validate<T> DebugInfo;

	static inline bool pass(Read&, T&) {
		return false;
	}
};

template<typename Read, typename T>
struct PassIf<Read, T, true> {
	static inline bool pass(Read& reader, T& x) {
		return reader.value(x);
	}
};

template<typename Read, typename T>
static inline bool pass(Read& r, T& x) {
	return PassIf<Read, T>::pass(r, x);
}

template<typename Read>
struct PassKeyIf {
	static inline bool pass(Read&, std::string&) {
		return false;
	}
};

template<typename Container>
struct PassKeyIf<ReadObject<Container> > {
	static inline bool pass(ReadObject<Container>& r, std::string& x) {
		return r.key(x);
	}
};

template<typename Read>
static inline bool passKey(Read& r, std::string& x) {
	return PassKeyIf<Read>::pass(r, x);
}

template<typename T, typename Tag, bool enable = couldBe<T, Tag>()>
struct CreateFor {
	typedef Validate<T> DebugInfo;

	template<typename Callback>
	static std::unique_ptr<Parser::Handler> create(const Callback&) {
		return nullptr;
	}
};

template<typename T, typename Tag>
struct CreateFor<T, Tag, true> {
	template<typename Callback>
	static std::unique_ptr<Parser::Handler> create(const Callback& c) {
		return std::unique_ptr<Parser::Handler>(new ReadHandler<T, Tag, Callback>(c));
	}
};

template<typename T, typename Tag, typename Callback>
std::unique_ptr<Parser::Handler> createFor(const Callback& c) {
	return CreateFor<T, Tag>::create(c);
}

} // namespace Detail
} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_DETAILS_PASS_HPP_
