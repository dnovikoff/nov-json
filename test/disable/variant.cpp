#include <test/test.hpp>
#include <nov/json/template/support/variant.hpp>

BOOST_AUTO_TEST_CASE( one ) {
	std::vector<boost::variant<int> > x;
	BOOST_REQUIRE( parseString("[6]", x ) );
	BOOST_CHECK_EQUAL(getOne<int>(x), 6);
}

BOOST_AUTO_TEST_CASE( two ) {
	// will try int first
	std::vector<boost::variant<int, double> > x;
	BOOST_REQUIRE( parseString("[4]", x ) );
	BOOST_CHECK_EQUAL(getOne<int>(x), 4);

	BOOST_REQUIRE( parseString("[6.5]", x ) );
	BOOST_CHECK_EQUAL(getOne<double>(x), 6.5);
}

BOOST_AUTO_TEST_CASE( order ) {
	// will try double first
	std::vector<boost::variant<double, int> > x;
	BOOST_REQUIRE( parseString("[6]", x ) );
	BOOST_CHECK_EQUAL(getOne<double>(x), 6);

	BOOST_REQUIRE( parseString("[6.5]", x ) );
	BOOST_CHECK_EQUAL(getOne<double>(x), 6.5);
}
