#ifndef NOV_JSON_TEMPLATE_SUPPORT_TUPLE_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_TUPLE_HPP_

#include <tuple>
#include <type_traits>

#include <nov/json/template/type.hpp>
#include <nov/json/template/read_array.hpp>
#include <nov/json/template/write.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<size_t I>
struct IndexIterator {
	static const size_t value = I;
	typedef IndexIterator<I + 1> next;
};

// Only declaration. Type is used only as template arg
struct IndexEndIterator;

template<typename TupleType, typename Iterator>
struct IndexHelper;

template<typename TupleType>
struct IndexHelper< TupleType, IndexEndIterator > {
	template<typename T>
	static bool apply(TupleType&, const size_t, T&) {
		return false;
	}
};

template<typename TupleType, typename Iterator>
struct IndexHelper {
	template<typename VisitorType>
	static bool apply(TupleType& t, const size_t index, VisitorType& visitor) {
		if (Iterator::value == index) {
			return visitor(std::get<Iterator::value>(t));
		}
		typedef typename Iterator::next NextIndex;
		static const bool more = ( NextIndex::value < std::tuple_size<TupleType>::value );
		typedef typename std::conditional<more, NextIndex, IndexEndIterator>::type NextIterator;
		return IndexHelper<TupleType, NextIterator>::apply(t, index, visitor);
	}
};

template<typename TupleType, typename ActionType>
static bool applyIndexHelper(TupleType& t, const size_t index, ActionType& act) {
	if (index >= std::tuple_size<TupleType>::value) {
		return false;
	}
	return IndexHelper<TupleType, IndexIterator<0u> >::apply(t, index, act);
}

template<typename J>
struct ApplyValue {
public:
	explicit ApplyValue(J& v): value_(&v) {
	}

	template<typename X>
	bool operator()(X& x) const {
		return jsonToUserOrFalse(*value_, x);
	}

private:
	J* value_;
};

template<typename Tag>
struct ApplyCreate {
public:
	template<typename X>
	bool operator()(X& x) {
		auto p = &x;
		handler_ = Detail::createFor<X, Tag>([p] (X&& in) {
			(*p) = std::move(in);
			return true;
		});
		return handler_.get();
	}

	std::unique_ptr<Parser::Handler> handler() {
		return std::move(handler_);
	}

private:
	std::unique_ptr<Parser::Handler> handler_;
};

template<typename... Types>
class ReadArray<std::tuple<Types...> > {
public:
	typedef std::tuple<Types...> TupleType;
	typedef Enum<Types...> Accepted;

	template<typename T>
	bool value(T& x) {
		ApplyValue<T> a(x);
		if (!applyIndexHelper(data_, index_, a)) {
			return false;
		}
		++index_;
		return true;
	}

	bool validate() const {
		return index_ == tupleSize;
	}

	template<typename Tag>
	std::unique_ptr<Parser::Handler> create() {
		ApplyCreate<Tag> a;
		applyIndexHelper(data_, index_, a);
		++index_;
		return a.handler();
	}

	TupleType move() {
		return std::move(data_);
	}

private:

	static const size_t tupleSize = std::tuple_size<TupleType>::value;

	size_t index_ = 0;
	TupleType data_;
};

template<bool enabled, size_t index, typename... Types>
struct TupleWriteHelper;

template<size_t index, typename... Types>
struct TupleWriteHelper<false, index, Types...> {
	template<typename Generator, typename T>
	static void write(Generator&, const T&) {}
};

template<bool enabled, size_t index, typename... Types>
struct TupleWriteHelper {
	typedef std::tuple<Types...> Type;

	template<typename Generator>
	static void write(Generator& g, const Type& x) {
		writeValue(g, std::get<index>(x));
		TupleWriteHelper<(index+1 < sizeof...(Types)), index+1, Types...>::write(g, x);
	}
};

template<typename... Types>
struct Writer<std::tuple<Types...> > {
	typedef std::tuple<Types...> Type;

	template<typename Generator>
	static void write(Generator& g, const Type& x) {
		g.startArray();
		TupleWriteHelper<sizeof...(Types)!=0, 0, Types...>::write(g, x);
		g.endArray();
	}
};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_TUPLE_HPP_
