#ifndef NOV_JSON_TEMPLATE_READ_ARRAY_HPP_
#define NOV_JSON_TEMPLATE_READ_ARRAY_HPP_

#include <nov/json/template/enum.hpp>

namespace Nov {
namespace Json {
namespace Template {

template<typename T>
class ReadArray {
public:
	typedef NothingTag Accepted;
};

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_READ_ARRAY_HPP_
