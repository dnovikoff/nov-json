/**
 * No support/vector.hpp header. Should fail - does not know how to deal with it
 */

#include <vector>
#include <nov/json/template/create.hpp>

void compile() {
	std::vector<int> mp;
	Nov::Json::Template::createHandler( mp );
}
