#include "test.hpp"

#include <nov/json/template/support/map_as_object.hpp>
#include <nov/json/template/support/tuple.hpp>
#include <nov/json/template/support/optional.hpp>
#include <nov/json/template/support/list.hpp>
#include <nov/json/template/support/pair.hpp>


template<typename T>
T& getFirst(std::tuple<T>& x) {
	return std::get<0>(x);
}

BOOST_AUTO_TEST_CASE( optinalContainer ) {
	std::tuple<boost::optional<std::vector<int> > > x;
	BOOST_REQUIRE( parseString("[null]", x ) );
	BOOST_CHECK( !getFirst(x).is_initialized() );

	BOOST_REQUIRE( parseString("[[]]", x ) );
	BOOST_REQUIRE( getFirst(x).is_initialized() );
	BOOST_REQUIRE( getFirst(x)->empty() );
}

