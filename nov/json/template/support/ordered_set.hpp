#ifndef NOV_JSON_TEMPLATE_SUPPORT_ORDERED_SET_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_ORDERED_SET_HPP_

#include <list>
#include <string>
#include <map>
#include <stdexcept>

#include <nov/json/template/support/ordered_base.hpp>

namespace Nov {
namespace Json {

struct OrderedSetKeyExtractPolicy {
	template<typename T>
	const T& operator()(const T& x) {
		return x;
	}
};

// Special class to read maps with correct order
template<typename Value>
class OrderedSet: public OrderedBase<Value, Value, OrderedSetKeyExtractPolicy> {
public:
	OrderedSet() {
	}

	template<typename Iter>
	OrderedSet(Iter begin, Iter end) {
		for (; begin != end; ++begin) {
			insert(*begin);
		}
	}

	OrderedSet(std::initializer_list<Value> l):OrderedSet(l.begin(), l.end()) {
	}
};

namespace Template {

template<typename... Args>
class ArrayInsert<OrderedSet<Args...> > {
public:
	typedef OrderedSet<Args...> Type;
	typedef typename Type::value_type ValueType;

	static bool insert(Type& data, ValueType& x) {
		return data.insert(std::move(x));
	}
};

NOV_JSON_ENABLE_ARRAY(OrderedSet)

} // namespace Template
} // namespace Json
} // namespace Nov

#endif // NOV_JSON_TEMPLATE_SUPPORT_ORDERED_SET_HPP_
