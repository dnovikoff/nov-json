#ifndef NOV_JSON_TEMPLATE_SUPPORT_DEQUE_HPP_
#define NOV_JSON_TEMPLATE_SUPPORT_DEQUE_HPP_

#include <deque>
#include <nov/json/template/support/helpers/list_like.hpp>

// Follows default insert for arrays
#define NOV_JSON_TEMPLATE_CONTAINER_NAME std::deque
#include <nov/json/template/support/helpers/list_macro.hpp>

#endif // NOV_JSON_TEMPLATE_SUPPORT_DEQUE_HPP_
